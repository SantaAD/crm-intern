<?php define('PROOT', 'http://localhost/crm') ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>CRM</title>


    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

    <meta charset="utf-8http://">
    <meta name="viewporthttp://" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-http://UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo PROOT ?>/resources/images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>/resources/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>/resources/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>/resources/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>/resources/css/style.css">
</head>

<body class="fix-menu">


    <section class="login-block">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <form class="md-float-material form-material" id="formLogin">
                        <div class="text-center">
                            <img src="<?php echo PROOT ?>/resources/images/logo.png" alt="logo.png" style="height:60px; width:80px">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-40">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Sign In</h3>
                                    </div>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="text" id="username" class="form-control"  placeholder="Your username">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" id="password" class="form-control"  placeholder="Password">
                                    <span class="form-bar"></span>
                                </div>

                                <div class="row m-t-40">
                                    <div class="col-md-12">
                                        <button type="submit" id="btn-submit" class="btn btn-inverse f-right btn-md waves-effect waves-light text-center m-b-30">Sign
                                            in</button>
                                    </div>
                                </div>
                                <div id="msg"></div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </section>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        const host = "http://localhost/crm";
        const formLogin = document.getElementById('formLogin');

        formLogin.addEventListener('submit', function(e) {
            e.preventDefault();

            let username = document.getElementById('username');
          
            let password = document.getElementById('password');

            let msgArea = document.getElementById('msg');

            let submitBtn = document.getElementById('btn-submit');

            submitBtn.innerHTML = 'PLEASE WAIT...';

            var params = new URLSearchParams();
            params.append('username', username.value);
            params.append('password', password.value);

            axios.post(host + '/auth/login.php', params)
                .then(function(response) {
                    console.log(response.data.msg);
                    if (response.data.status !== 'success') {
                        submitBtn.innerHTML = 'SIGN IN';
                        msgArea.innerHTML = ` <div class="alert alert-danger alert-fill-danger" role="alert">
                                              <i class="mdi mdi-alert-circle"></i>
                                              ${response.data.msg}
                                            </div>`;
                        return;
                    }

                    window.location = './dashboard.php';
                })
                .catch(function(error) {
                    submitBtn.innerHTML = 'SIGN IN';
                    console.log(error);
                });
        })
    </script> 