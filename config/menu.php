<?php
$menus = [
    ['name' => 'dashboard', 'path' => './dashboard.php', 'icon' => 'mdi mdi-home menu-icon'],
    ['name' => 'customer', 'path' => './customer/index.php', 'icon' => 'mdi mdi-account-heart menu-icon'],
    ['name' => 'sales', 'path' => './sales/index.php', 'icon' => 'mdi mdi-currency-inr menu-icon'],
    ['name' => 'amc', 'path' => './amc/index.php', 'icon' => 'mdi mdi-stocking menu-icon']
];