<?php
/**
 * Configuration file
 * This file contains all the configuration for the system
 * Mofiqul Islam , mofi0islam@gmail.com
 * 
 * Created at : 23-02-2019 02:09PM 
 */

# MYSQL Database
define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', 'password');
define('DBNAME', 'crm');


# Project root
define('DOC_ROOT', '/home/buildozer/code/crm');