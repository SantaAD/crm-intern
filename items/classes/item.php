<?php

use PHPUnit\Framework\MockObject\Stub\Exception;
class Item 
{
    public function __construct($item = null)
    {
        $this->db = Database::getInstance();

        if( $item == null ) return $this;

        $this->populateObject($item);

        return $this;
    }



    public function save()
    {
        $model = isset($this->model) ? $this->model : '';
        $serial = isset($this->serial) ? $this->serial : '';
        $barcode = isset($this->barcode) ? $this->barcode : '';

        $sql = "insert into items ( `name`, `price`, `model`, `serial_no`, `barcode`) values ('$this->name', '$this->price', '$model', '$serial', '$barcode'); ";

        if( ! $res = $this->db->query($sql) ){
            throw new Exception($this->db->error);
        } 

        $this->id = $this->db->insert_id;

        return $this>findById($this->id);
    }

    


    public function findById($id)
    {
        $sql = "SELECT * FROM items WHERE id = '$id'";

        if(! $res = $this->db->query($sql)){
            throw new Exception($this->db->error);
        }

        return $this->populateObject($res->fetch_object());
    }

    public function findByBarcode($barcode)
    {
        $sql = "SELECT * FROM items WHERE barcode = '$barcode'";

        if(! $res = $this->db->query($sql)){
            throw new Exception($this->db->error);
        }

        return $this->populateObject($res->fetch_object());
    }

    public function populateObject($result)
    {
        foreach($result as $key => $value){
            $this->$key = $value;
        }

        return $this;
    }


    public function delete()
    {

        $sql = "DELETE FROM items WHERE id = '$this->id'";
        return $this->db->query($sql);

    }
}