<?php
require_once('../../libs/database/database.php');

$db = Database::getInstance();
$input = $_POST['input'];

if(strlen($input) < 1){
    echo json_encode([
        "msg" => "No Item found"
    ]);
    return;
}

$sql = "SELECT `id`, `name`, `price`, `size` FROM `items` WHERE `name` LIKE '%$input%'";

$res = $db->query($sql);

if($res->num_rows < 1){
    echo json_encode([
        "msg" => "No Item found"
    ]);
    return;
}

$items = [];

while($row = $res->fetch_object()){
    $items[] = $row;
}

echo (json_encode([
    "items" => $items
]));