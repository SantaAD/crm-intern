<?php

require_once '../../libs/database/database.php';
require_once '../classes/item.php';

$item_id = isset($_POST['item-id']) ? $_POST['item-id'] : '';
$barcode = isset($_POST['barcode']) ? $_POST['barcode'] : '';
$item = new Item();
try{
    
    if(strlen($barcode) < 1){
        $item = $item->findById($item_id);
        echo json_encode(['item' => $item]);
    } else {
        $item = $item->findByBarcode($barcode);
        echo json_encode(['item' => $item]);
    }
    

} catch(Exception $e){

    echo json_encode(['msg' => "Item not found"]);

}

