<?php
include '../layout/header.php';
include '../layout/sidebar.php';

$id =  $_GET['sale'];

require_once('../libs/database/database.php');
require_once('./interfaces/interface_sale.php');
require_once('./classes/sale.php');
require_once('../items/classes/item.php');
require_once('../customer/Customer.php');

$sale = new Sale();

$sale = $sale->findByID($id);

$sale_item = $sale->getItems();

$customer = new Customer();

$customer = $customer->findByID($sale->customer_id);
$address = $customer->getAddress();

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body" id="invoice">
                    <div class="row">
                        <div class="container">
                            <div>
                                <div class="card">
                                    <div class="row invoice-contact">
                                        <div class="col-md-12">
                                            <div class="invoice-box row">
                                                <div class="col-sm-12">
                                                    <table class="table table-responsive invoice-table table-borderless" style="width:100%; text-align:center; margin-top:-20px;">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width:90%">
                                                                    <strong style="font-size:30px; color:salmon">Trans Technologies Solution Pvt. Ltd.</strong><br>
                                                                    <strong> Tel: +91-7526547852 (O), 9876543210 (M)</strong><br>
                                                                    <strong><a target="_top"><span class="__cf_email__">e-mail: sales@transt.co.in</span></a></strong><br>
                                                                    <strong>http://companyurl.com</strong>
                                                                </td>
                                                                <td><img src="<?php echo PROOT ?>/resources/images/logo-blue.png" style="height:60px; width:100px; float:right" class="m-b-10" alt=""></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="row invoive-info">
                                            <table class="table " style="margin-top:-50px">
                                                <tr>
                                                    <td>
                                                        <div class="col-md-12 col-xs-12 invoice-client-info">
                                                            <h6>Client Information :</h6>
                                                            <h6 class="m-0">
                                                                <?php echo $customer->name ?>
                                                            </h6>
                                                            <p class="m-0 m-t-10">
                                                                <?php echo $address->address . ', ' . $address->city . '-' . $address->pin ?>
                                                            </p>
                                                            <p class="m-0">
                                                                <?php echo $customer->phone ?>
                                                            </p>
                                                            <p><a href="/cdn-cgi/l/email-protection" class="__cf_email__">
                                                                    <?php echo $customer->email ?></a></p>

                                                            <p><strong>GSTIN:</strong> <?php echo $customer->gstin ?></p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-12 col-sm-12">
                                                            <h6>Order Information :</h6>
                                                            <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Date :</th>
                                                                        <td>
                                                                            <?php echo $sale->date ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Status :</th>
                                                                        <td>
                                                                            <span class="label label-success">Paid</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-12 col-sm-12">
                                                            <h6 class="m-b-20">Invoice Number: <span>
                                                                    <?php echo $sale->id ?></span></h6>
                                                            <h6 class="text-uppercase text-primary">Total Due :
                                                                <span>
                                                                    <?php echo $sale->total ?></span>
                                                            </h6>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>



                                        </div>
                                        <div class="row" style="margin-top:-50px">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table invoice-detail-table">
                                                        <thead>
                                                            <tr class="thead-default">
                                                                <th style="width:30px">Sl.</th>
                                                                <th>Properties</th>
                                                                <th>Quantity</th>
                                                                <th>Amount</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0;
                                                            foreach ($sale_item as $item) : ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $i + 1; ?>
                                                                </td>
                                                                <td style="text-align:left">
                                                                    <?php echo $item->name . ',   ' . $item->size ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $item->quantity ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $item->price ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $item->quantity * $item->price ?>
                                                                </td>
                                                            </tr>
                                                            <?php $i++;
                                                        endforeach ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table table-responsive invoice-table invoice-total">
                                                    <tbody>
                                                        <tr>
                                                            <th>Sub Total :</th>
                                                            <td>
                                                                <?php echo $sale->sub_total ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>GST :</th>
                                                            <td>
                                                                <?php echo $sale->gst_total ?>
                                                            </td>
                                                        </tr>

                                                        <tr class="text-info">
                                                            <td>
                                                                <hr />
                                                                <h5 class="text-primary">Total :</h5>
                                                            </td>
                                                            <td>
                                                                <hr />
                                                                <h5 class="text-primary">
                                                                    <?php echo $sale->total ?>
                                                                </h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h6>Terms And Condition :</h6>
                                                <p>lorem ipsum dolor sit amet, consectetur adipisicing
                                                    elit, sed do eiusmod tempor incididunt ut labore et
                                                    dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut
                                                    aliquip ex ea commodo consequat. Duis aute irure
                                                    dolor </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row text-center">
                                    <div class="col-sm-12">
                                        <button type="button" id="btn-print" class="btn btn-primary btn-print-invoice">Print</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include '../layout/footer.php';
?>

<script>
    const btnPrint = document.getElementById('btn-print');
    btnPrint.addEventListener('click', function() {
        let body = document.getElementById('pcoded');
        let bodyBackup = body.innerHTML;
        btnPrint.style.display = "none";
        let invoice = document.getElementById('invoice').innerHTML;
        body.innerHTML = invoice;

        window.print();
        body.innerHTML = bodyBackup;
        btnPrint.style.display = "block";
    })
</script> 