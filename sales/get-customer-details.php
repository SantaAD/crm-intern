<?php
require_once ('../libs/database/database.php');
require_once '../customer/Customer.php';

$id = $_POST['id'];

$cust = new Customer();
$cust = $cust->findByID($id);
$add = $cust->getAddress();

$detail = [
    'id' => $cust->id,
    'name' => $cust->name,
    'phone' => $cust->phone,
    'email' => $cust->email,
    'address' => $add->address.', '.$add->pin.', '.$add->city
];

echo json_encode(['customer'=> $detail ]);