let searchCustomer = document.getElementById('search-customer');

let dataList = document.getElementById('customers');


searchCustomer.addEventListener('keyup', function (event) {
    let value = event.target.value;
    let input = new URLSearchParams();
    input.append('input', value);
    dataList.innerHTML = '';
    axios.post(config.host + 'sales/get-customer-list.php', input)
        .then(response => {

            for (let i = 0; i < response.data.customers.length; i++) {

                let option = `<option value="${response.data.customers[i].id}"> ${response.data.customers[i].id}, ${response.data.customers[i].name}</option>`;

                dataList.innerHTML += option;
            }

        })
        .catch(error => {
            console.log(error);
        })
})

dataList.addEventListener('change', function (event) {
    let customerId = event.target.value;

    let params = new URLSearchParams();
    params.append('id', customerId);

    axios.post(config.host + 'sales/get-customer-details.php', params)
        .then(response => {
            console.log(response.data.customer);
            document.getElementById('client-id').innerHTML = response.data.customer.id;
            document.getElementById('client-name').innerHTML = response.data.customer.name;
            document.getElementById('client-address').innerHTML = response.data.customer.address;
            document.getElementById('client-phone').innerHTML = response.data.customer.phone;
            document.getElementById('client-email').innerHTML = response.data.customer.email;

        })
        .catch(error => {
            console.log(error);
        })

});

function numberFormat(number) {
    if (typeof (number) != 'Number') {
        number = Number(number);
    }

    return number.toFixed(2);
}

dataList.addEventListener('change', function (event) {
    let value = event.target.value;
    //event.target.style = "display:none";
    //searchCustomer.style = "display:none"
    console.log(value);
})

function selectRow(row) {

    if (row.className == 'highlight') {
        row.className = ""
        row.style.backgroundColor = "";
    } else {
        row.className = "highlight";
        row.style.backgroundColor = "yellow";
    }

}

const deleteRow = document.getElementById('delete-row');

deleteRow.addEventListener('click', function () {
    let rows = document.getElementsByClassName('highlight');
    const table = document.getElementById('invoice-table');

    for (let i = 0; i < rows.length; i++) {

        table.deleteRow(rows[i].rowIndex);
    }

    calculateTotal();
    calculateGrandTotal();
})

function calculateGrandTotal() {
    let subTotal = document.getElementById('sub-total').innerHTML;
    let gstTotal = document.getElementById('gst-total').innerHTML;

    subTotal = subTotal.replace(/,/g, '');;
    gstTotal = gstTotal.replace(/,/g, '');;

    let grandTotal = Number(subTotal) + Number(gstTotal);

    document.getElementById('grand-total').innerHTML = grandTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function calculateTotal() {
    const tableRows = document.getElementById('invoice-table').rows;
    let sum = 0;
    gstAmount = 0;
    for (let i = 0; i < tableRows.length; i++) {
        if (i == 0) continue;
        let amount = Number(tableRows[i].cells[4].innerHTML)
        sum += amount
        let gstPercent = Number(tableRows[i].cells[5].innerHTML) / 100;
        console.log(gstPercent)
        gstAmount += amount * gstPercent ;

        //sum += gstAmount;

        document.getElementById('sub-total').innerHTML = sum.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

        document.getElementById('gst-total').innerHTML = gstAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        console.log('GST :' +gstAmount + 'Total:' +sum);
    }

}



function updateAmount(btn) {
    let qty = btn.innerHTML;
    let priceAmount = $(btn).closest('tr').find('.price-amount').text();
    let totalAmount = $(btn).closest('tr').find('.total-amount').text();

    let gst = $(btn).closest('tr').find('.gst').text();

    let newTotal = 0;

    newTotal = Number(qty) * Number(priceAmount);

    //gstAmount = newTotal * Number(gst) / 100;

    newTotal = newTotal ;
    $(btn).closest('tr').find('.total-amount').text(numberFormat(newTotal));
    //console.log('qty %s ,price %s, total %s, gst %s', qty, priceAmount, totalAmount, gst);

    calculateTotal();
    calculateGrandTotal();
}


function getValue(btn) {
    //console.log(event.target.value)
    let itemArea = document.getElementById('table-invoice');
    params = new URLSearchParams();
    params.append('item-id', btn.value);
    axios.post(config.host + 'items/api/get-item-details.php', params)
        .then(response => {

            //let gst = response.data.item.price * (response.data.item.gst / 100);

            let tr = `
                    <tr onclick="selectRow(this)">
                        <td style="display:none">${response.data.item.id}</td>
                        <td style="text-align:left; padding-left:10px">${response.data.item.name}</td>
                        <td contenteditable oninput="updateAmount(this)" class="quantity" >1</td>
                        <td class="price-amount">${numberFormat(response.data.item.price)}</td>
                        <td class="total-amount">${numberFormat(response.data.item.price * 1 )}</td>
                        <td class="gst" style="display:none">${response.data.item.gst}</td>
                    </tr>
        `;
            itemArea.innerHTML += tr;
            btn.disabled = "disabled";

            calculateTotal();
            calculateGrandTotal();
        })
        .catch(error => {
            console.log(error);
        })
}

const searchItem = document.getElementById('search-item');

const itemTableBody = document.getElementById('item-table-body');

searchItem.addEventListener('keyup', (event) => {
    let value = event.target.value;

    let input = new URLSearchParams();
    input.append('input', value);
    itemTableBody.innerHTML = '';
    axios.post(config.host + 'items/api/get-item-list.php', input)
        .then(response => {
            if (!response.data.items) {
                itemTableBody.innerHTML = '<td> ' + response.data.msg + '</td>'
                return;
            }
            for (let i = 0; i < response.data.items.length; i++) {
                let row = `
                <tr class="selected-item" style="cursor:pointer">
                    <td>${response.data.items[i].name}</td>
                    <td>${response.data.items[i].price}</td>
                    <td>${response.data.items[i].size}</td>
                    <td><button onclick="getValue(this)" class="btn btn-sm btn-success" value="${response.data.items[i].id}">
                        <i style="font-size:20px" class="feather icon-plus-circle"></i>
                    </button></td>
                </tr>
            `;

                itemTableBody.innerHTML += row;
            }
        })
        .catch(error => {
            console.log(error);
        })

    
    function tableToJson(table) {
        let headers = [];
        var data = []; // first row needs to be headers var headers = []; 
        for (var i = 0; i < table.rows[0].cells.length; i++) {
            headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
        }
        // go through cells 
        for (var i = 1; i < table.rows.length; i++) {
            var tableRow = table.rows[i];
            var rowData = {};

            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }

            data.push(rowData);
        }
        return data;
    }

const btnInvoice = document.getElementById('submit-invoice');

btnInvoice.addEventListener('click', function(){
    const table = document.getElementById('invoice-table');
   
    let customerId = document.getElementById('client-id').innerText;
    let totalAmount = document.getElementById('grand-total').innerText;
    let subTotal = document.getElementById('sub-total').innerText;
    let totalGST = document.getElementById('gst-total').innerHTML;

    if(customerId.length < 1){
        alert("Please select customer");
        return;
    }
    if(totalAmount == '00.00'){
        alert("Please add some items");
        return;
    }

    let json = tableToJson(table);

    let data = {
        "products": json,
        "client-id": customerId,
        "sub-total": subTotal,
        "gst-total": totalGST,
        "total": totalAmount
    }
    
    console.log(data);

    axios.post(config.host + 'sales/save-sale.php', {"data": data})
        .then(response =>{
            console.log(response.data);
            if(response.data.status == 'success'){
                let saleID = response.data.sale_id;
                window.location = config.host + 'sales/checkout.php?sale='+saleID;
            } else{
                alert(response.data.msg);
            }
        })
        .catch(error => {
            console.log(error);
        })
})

})