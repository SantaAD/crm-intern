<?php
//session_start();
$_SESSION['user'] = 'Admin';
include '../layout/header.php';
include '../layout/sidebar.php';
require_once('../libs/database/database.php');
require_once('../libs/session/session.php');
require_once './interfaces/interface_sale.php';
require_once './classes/sale.php';
$sale = new Sale();

if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $sale_to_delete = $sale->findByID($id);
    $sale_to_delete->delete();
    echo '<script>window.location = "./index.php?page=1"</script>';
}

try{
    if(isset($_POST['search'])){
        if(strlen($_POST['search-by']) > 1){
            $sales = $sale->get(1, $_POST['search-by'], $_POST['search-value']);
        } else if(strlen($_POST['from']) > 1 && strlen($_POST['to']) > 1) {
            $sales = $sale->getByDate(1, $_POST['from'], $_POST['to']);
        } else{
            $sales = $sale->get();
        }
        
    } else{
        $sales = $sale->get($_GET['page']);
    }
} catch(Exception $e){
    $sales = [];
}


$total_sale = $sale->getSellCount();

$total_pages = ceil($total_sale) / 20 > 1 ? ceil($total_sale) / 20 : 1 ;

?>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">


                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="row">
                                        <div class="text-center">
                                             <a href="<?php echo PROOT ?>/sales/sale.php?>" class="btn btn-tumblr btn-mat">
                                        New Sale
                                    </a>
                                        </div>
                                        <div class="col col-lg-9">
                                           <h3 style="float:right">Sale Details</h3>
                                        </div>
                                    </div>
                            <div class="page-header-title">
                               
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Filter options</h5>
                                    <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">

                                        <div class="form-group row">
                                            <div class="col col-sm-12 col-md-2">
                                                <select name="search-by" id="" class="form-control sm">
                                                    <option value="">--SELECT--</option>
                                                    <option value="id">ID</option>
                                                    <option value="name">Customer Name</option>
                                                    <option value="seller">Sale By</option>
                                                </select>
                                            </div>

                                            <div class="col col-sm-12 col-md-4">
                                                <input name="search-value" type="text" class="form-control sm" placeholder="Search">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <input name="from" type="text" class="form-control sm" placeholder="From">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <input name="to" type="text" class="form-control sm" placeholder="To">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <button type="submit" name="search" class="btn btn-tumblr btn-sm btn-mat" style="font-size:18px;"><i class="feather icon-filter"></i></button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">

                                        <table id="simpletable" class="table table-xs  nowrap table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Cutomer</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                    <th>Sale By</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($sales as $sale) : ?>

                                                <tr>
                                                    <td>
                                                        <?php echo $sale->id ?>
                                                    </td>
                                                    <td><a class="text-primary" href="<?php echo PROOT?>/customer/profile.php?profileview=<?php echo $sale->customer_id?>">
                                                            <?php echo $sale->getCustName() ?></a></td>
                                                    <td>
                                                        <?php echo $sale->total ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $sale->date ?>
                                                    </td>
                                                    <td>
                                                        <?php echo Session::get('user') ?>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown-primary dropdown open">
                                                            <button class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light " type="button" id="dropdown-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Actions</button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdown-2" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                                <a class="dropdown-item waves-light waves-effect" href="<?php echo PROOT ?>/sales/view.php?sale=<?php echo $sale->id ?>">
                                                                    <i class="text-success feather icon-eye"></i>
                                                                    View
                                                                </a>
                                                                <div class="dropdown-divider"></div>
                                                                <a onclick="return confirm('Are you sure');" class="dropdown-item waves-light waves-effect" href="<?php echo $_SERVER['PHP_SELF'] ?>?delete=<?php echo $sale->id ?>">
                                                                    <i class="text-danger feather icon-trash-2"></i>
                                                                    Delete
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <?php endforeach ?>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <nav aria-label="..." style="float:right">
                                <ul class="pagination">

                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>

                                    <?php for($i = 0; $i < $total_pages; $i++):?>

                                    <li class="page-item <?php echo $i + 1 == $_GET['page']? 'active': null?>"><a class="page-link" href="<?php echo $_SERVER['PHP_SELF']?>?page=<?php echo $i + 1?>"><?php echo $i + 1?></a></li>

                    
                                    <?php endfor?>

                                    <li class="page-item">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
include '../layout/footer.php';
?> 