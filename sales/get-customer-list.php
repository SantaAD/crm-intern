<?php
require_once('../libs/database/database.php');
$db = Database::getInstance();

$input = $_POST['input'];

if(strlen($input) < 1){
    [
        'customers' => ['id' => null, 'name' => 'No customer found']
    ];
    return;
};

$sql = "SELECT id, name FROM customer WHERE name LIKE '%$input%'";


$res = $db->query($sql);

if($res->num_rows < 1){
    echo json_encode([
        'customers' => ['id' => null, 'name' => 'No customer found']
    ]);
    return;
}

$customers = [];

while($row = $res->fetch_object()){
    $customers[] = $row;
}

 echo json_encode(['customers' => $customers]);