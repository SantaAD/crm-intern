<?php
    include '../layout/header.php';
    include '../layout/sidebar.php';
?>
           
           <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="container">
                                            <div>
                                                <div class="card">
                                                    <div class="row invoice-contact">
                                                        <div class="col-md-8">
                                                            <div class="invoice-box row">
                                                                <div class="col-sm-12">
                                                                    <table class="table table-responsive invoice-table table-borderless">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td><img src="<?php echo PROOT?>/resources/images/logo-blue.png"
                                                                                        class="m-b-10" alt=""></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Trans Technologies Solution Pvt. Ltd.</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Zoo Tiniali, Ghy-24</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong><a href="/cdn-cgi/l/email-protection#c3a7a6aeac83a4aea2aaafeda0acae"
                                                                                        target="_top"><span class="__cf_email__"
                                                                                            data-cfemail="d3b7b6bebc93b4beb2babffdb0bcbe">[email protected]</span></a></strong>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>+91-7526547852</strong></td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="row invoive-info">
                                                            <div class="col-md-4 col-xs-12 invoice-client-info">
                                                                <h6>Client Information :</h6>
                                                                <h6 class="m-0">Mofiqul Khatun</h6>
                                                                <p class="m-0 m-t-10">Bye Lane 3, Anil Nagar, Bhangagarh</p>
                                                                <p class="m-0">+91-9999999900</p>
                                                                <p><a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                                        data-cfemail="ddb9b8b0b29da5a4a7f3beb2b0">[email protected]</a></p>
                                                            </div>
                                                            <div class="col-md-4 col-sm-6">
                                                                <h6>Order Information :</h6>
                                                                <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th>Date :</th>
                                                                            <td>November 14</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Status :</th>
                                                                            <td>
                                                                                <span class="label label-success">Delivered</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Id :</th>
                                                                            <td>
                                                                                #145698
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-4 col-sm-6">
                                                                <h6 class="m-b-20">Invoice Number <span>#12398521473</span></h6>
                                                                <h6 class="text-uppercase text-primary">Total Due :
                                                                    <span>&#8377 900.00</span>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="table-responsive">
                                                                    <table class="table invoice-detail-table">
                                                                        <thead>
                                                                            <tr class="thead-default">
                                                                                <th style="width:30px">Sl.</th>
                                                                                <th>Properties</th>
                                                                                <th>Quantity</th>
                                                                                <th>Amount</th>
                                                                                <th>Total</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>1</td>
                                                                                <td style="text-align:left">
                                                                                    <h6>Logo Design</h6>
                                                                                    <p>lorem ipsum dolor sit amet,
                                                                                        consectetur adipisicing elit,
                                                                                        sed do eiusmod tempor
                                                                                        incididunt </p>
                                                                                </td>
                                                                                <td>6</td>
                                                                                <td>&#8377 200.00</td>
                                                                                <td>&#8377 1200.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>2</td>
                                                                                <td style="text-align:left">
                                                                                    <h6>Logo Design</h6>
                                                                                    <p>lorem ipsum dolor sit amet,
                                                                                        consectetur adipisicing elit,
                                                                                        sed do eiusmod tempor
                                                                                        incididunt </p>
                                                                                </td>
                                                                                <td>6</td>
                                                                                <td>&#8377 200.00</td>
                                                                                <td>&#8377 1200.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>3</td>
                                                                                <td style="text-align:left">
                                                                                    <h6>Logo Design</h6>
                                                                                    <p>lorem ipsum dolor sit amet,
                                                                                        consectetur adipisicing elit,
                                                                                        sed do eiusmod tempor
                                                                                        incididunt </p>
                                                                                </td>
                                                                                <td>6</td>
                                                                                <td>&#8377 200.00</td>
                                                                                <td>&#8377 1200.00</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table class="table table-responsive invoice-table invoice-total">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th>Sub Total :</th>
                                                                            <td>&#8377 4725.00</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>GST (18%) :</th>
                                                                            <td>&#8377 57.00</td>
                                                                        </tr>
                                                                        
                                                                        <tr class="text-info">
                                                                            <td>
                                                                                <hr />
                                                                                <h5 class="text-primary">Total :</h5>
                                                                            </td>
                                                                            <td>
                                                                                <hr />
                                                                                <h5 class="text-primary">&#8377 4827.00</h5>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <h6>Terms And Condition :</h6>
                                                                <p>lorem ipsum dolor sit amet, consectetur adipisicing
                                                                    elit, sed do eiusmod tempor incididunt ut labore et
                                                                    dolore magna aliqua. Ut enim ad minim veniam, quis
                                                                    nostrud exercitation ullamco laboris nisi ut
                                                                    aliquip ex ea commodo consequat. Duis aute irure
                                                                    dolor </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row text-center">
                                                    <div class="col-sm-12 invoice-btn-group text-center">
                                                        <button type="button" class="btn btn-primary btn-print-invoice m-b-10 btn-sm waves-effect waves-light m-r-20">Print</button>
                                                        <button type="button" class="btn btn-danger waves-effect m-b-10 btn-sm waves-light">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<?php
    include '../layout/footer.php';
?>
