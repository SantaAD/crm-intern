<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <form action="">
                                        <div class="form-group">
                                            <label for="search-customer">Search customer</label>
                                            <input type="text" list="customers" id="search-customer" class="form-control" placeholder="Search customer">
                                            <!--<table id="customers" class="table table-hover"></table>-->
                                        </div>
                                        <div class="form-group">
                                            <select name="" style="font-size:20px; padding:10px 10px" id="customers" multiple class="form-control">

                                            </select>
                                            <!--<table id="customers" class="table table-hover"></table>-->
                                        </div>
                                    </form>

                                    <!-- item search -->
                                    <form>
                                        <div class="form-group row">
                                            <div class="col col-md-6">
                                                <input type="text" id="search-item" class="form-control" placeholder="Search items">
                                            </div>
                                            <div class="col col-md-6">
                                                <input type="text" id="search-item" class="form-control" placeholder="Scan barcode">
                                            </div>

                                        </div>
                                    </form>


                                    <!-- item table-->
                                    <table id="item-table" class="table table-hover">
                                        <thead>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Add</th>
                                        </thead>
                                        <tbody id="item-table-body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="row invoive-info">
                                            <div class="col-md-12 col-xs-12 invoice-client-info">
                                                <h6>Client Information :</h6>

                                                <h6 id="client-name" class="m-0"> </h6>
                                                <p id="client-id" style="display:none"></p>
                                                <p id="client-address" class="m-0 m-t-10"></p>
                                                <p id="client-phone" class="m-0"></p>
                                                <p><a href="/cdn-cgi/l/email-protection" id="client-email" class="__cf_email__" data-cfemail="ddb9b8b0b29da5a4a7f3beb2b0"></a></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive" style="heeight:200px; margin-top:-40px;overflow-y:scroll;">
                                                    <table class="table" id="invoice-table" style="width:100%; text-align:center;">
                                                        <thead>
                                                            <tr class="thead-default">
                                                                <th style="display:none">id</th>
                                                                <th style="text-align:center">Properties</th>
                                                                <th style="text-align:center">Quantity</th>
                                                                <th style="text-align:center">Amount</th>
                                                                <th style="text-align:center">Total</th>
                                                                <th style="display:none">GST</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="table-invoice">
                                                            
                                                        </tbody>
                                                    </table>
                                                    <button id="delete-row"  class="btn btn-danger btn-sm">
                                                        <i style="font-size:20px" class="feather icon-x-circle"></i>
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table table-responsive invoice-table invoice-total">
                                                    <tbody>
                                                        <tr>
                                                            <th>Sub Total :</th>
                                                            <td id="sub-total">00.00</td>
                                                        </tr>
                                                        <tr>
                                                            <th>GST (18%) :</th>
                                                            <td id="gst-total">00.00</td>
                                                        </tr>

                                                        <tr class="text-info">
                                                            <td>
                                                                <hr />
                                                                <h5 class="text-primary">Total :</h5>
                                                            </td>
                                                            <td>
                                                                <hr />
                                                                <h5 id="grand-total" class="text-primary">00.00</h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row text-center">
                                    <div class="col-sm-12 invoice-btn-group text-center" style="margin-bottom:10px; text-align:right">
                                        <button id="submit-invoice" type="button" class="btn btn-tumblr">Checkout</button>
                                        <button type="button" class="btn btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> 