<?php
/**
 * Interface sale 
 * All the sale class will impliments this class
 * 
 */
interface interfaceSale
{
    public function save();

    public function update($data);

    public function findByID($id);

    public function find($queryParams);

    public function delete();
}

