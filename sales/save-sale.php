<?php
$_POST = json_decode(file_get_contents("php://input"),true);
$data = $_POST['data'];

$products = $data['products'];


require_once('../libs/database/database.php');
require_once('./interfaces/interface_sale.php');
require_once('./classes/sale.php');

$sale_data = [
    "customer_id" => $data['client-id'],
    "sub_total" => $data['sub-total'],
    "gst_total" => $data['gst-total'],
    "total" => $data['total']
];

$sale = new Sale($sale_data);
try{
    $saved_sale = $sale->save();

    foreach($products as $product){
        $item = [
            'id' => $product['id'],
            'qty' => $product['quantity']
        ];

        $saved_sale->saveItem($item);
    }

    echo json_encode([
        'status' => 'success', 
        'sale_id' => $saved_sale->id
        ]
    );
} catch(Exception $e){
    echo $e->getMessage();
    echo json_encode([
        'status' => 'failed',
        'msg' => 'Failed to save sales details'
    ]);
}



