<?php



/**
 * Class sale
 */

 class Sale implements interfaceSale
 {
    

    protected $db = null;

    /**
     * Creates a new sale and returns the sale object
     * @param array $sale
     * @return Sale $sale
     */
    public function __construct($sale = null)
    {
        $this->db = Database::getInstance();

        if(!$sale){
             return $this;
        } 

        foreach($sale as $key => $value){
            $this->$key = $value;
        }

        return $this;
    }

    /**
     * Saves a sale to database
     * @return Sale $sale
     */
    public function save()
    {
        $this->id = $this->generateID();
        $sql = "INSERT INTO sale 
                (`id`, `sub_total`, `gst_total`, `total`, `customer_id`) VALUES ('$this->id','$this->sub_total', '$this->gst_total', '$this->total', '$this->customer_id')";

        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);
    }



    /**
     * Generates sale id
     * @return interger $sale_id
     */
    private function generateID(){

        $sql = "SELECT max(id) as max_id FROM sale";

        $res = $this->db->query($sql);

        $maxID = $res->fetch_object()->max_id;

        if($maxID == 0){
            return $maxID + 100000;
        }

        return $maxID + 1;
    }

    /**
     * Find sale by id
     * @param integer $id
     * @return Sale $sale
     */
    public function findByID($id)
    {
        if(!$id){
            throw new Exception('Missing amc id');
        }

        $sql = "SELECT * FROM sale WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        return $this->populateObject( $res->fetch_object() );
    }

    /**
     * Find sale by query
     * @param Array $queryParams
     * @return Array $sales
     */
    public function find($queryParams)
    {
        $key = key($queryParams);

        $sql = "SELECT `id`, `customer_id` FROM sale WHERE `{$key}` = '$queryParams[$key]' ";

        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;
        $rows = [];
        while($row = $res->fetch_object()){
            $rows[] = $row;
        } 
        return $rows;

    }

    /**
     * Updates a existing sale in database
     * Takes key value pair 
     * @param array $data
     * @return Sale $updaed_sale
     */
    public function update($data){

        if(!$data){
            throw new Exception('Missing fileds');
        }

        foreach($data as $key => $value){
            $this->{$data[$key]} = $value;
        }

        $sql = "UPDATE sale SET  `customer_id` = '$this->customer_id' WHERE id = '$this->id'";

        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);

    }

    /**
     * Sets all the filed name of table row as Sale property
     *
     * @param Object $result
     * @return Sale $sale
     */
    private function populateObject($result)
    {
       $result = (array) $result;
       foreach($result as $key => $value){
           $this->$key = $value;
       }
       return $this;
    }

    public function get($page_no = 1, $search_by = '', $value = ''){
        
        $page_size = 20;
        $current_page = $page_no - 1; 

        $skip = $current_page * $page_size;

        $up_limit = $skip + $page_size;
        $sale = [];
        $sales = [];


        if($search_by == 'id'){

            $sql = "SELECT * FROM sale WHERE id = '$value'";

        } elseif($search_by == 'name'){

            $sale = $this->searchByName($value);

        } elseif($search_by == 'seller'){

            $sale = $this->searchBySeller($value);

        } else{

            $sql = "SELECT * FROM sale LIMIT $skip , $up_limit";

        }

        if(count($sale) > 0){
            foreach($sale as $s){
                $sales[] = new Sale($s);
            }
            return $sales;
        }
        

        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception($this->db->error);
        }
        
        while($row = $res->fetch_object()){
            
            $sales[] = new Sale($row);

        }
        
        return $sales;
    }

    /**
     * Returns the customer name of the sale
     *
     * @return String 
     */
    public function getCustName(){
        $sql = "SELECT name FROM customer WHERE id = '$this->customer_id'";
        $res = $this->db->query($sql);
        return $res->fetch_object()->name;
    }

    /**
     * Delete sale from database
     * @return void
     */
    public function delete(){
        $sql = "DELETE FROM sale WHERE id = '$this->id'";
        return ($this->db->query($sql));
    }


    public function saveItem($item)
    {
        $item_id = $item['id'];
        $qty = $item['qty'];

        $sql = "INSERT INTO sale_items (`item_id`, `quantity`, `sale_id`) VALUES ('$item_id', '$qty', '$this->id')";

        return $this->db->query($sql); 
        
    } 


    public function getItems()
    {
        $sql = "SELECT item_id, quantity FROM sale_items WHERE sale_id = '$this->id'";

        //echo $sql; die;

        $res = $this->db->query($sql);
        $items = [];

        while($row = $res->fetch_object()){
            $item = new Item();
            $item = $item->findByID($row->item_id);
            $item->quantity = $row->quantity;
            $items[] = $item;
        }

        return $items;
    }
    
    public function getSellCount(){
        $sql = "SELECT count(id) as total_sale FROM sale";
        $res = $this->db->query($sql);

        return $res->fetch_object()->total_sale;
    }

    public function searchByName($name){
        $ids = $this->getCustIds($name);
        foreach($ids as $id){

            $sql = "SELECT * FROM sale WHERE customer_id = '$id'";
            $res = $this->db->query($sql);

            if($res->num_rows < 1){
                throw new Exception($this->db->error);
            }

            $sales = [];

            while( $row = $res->fetch_object() ){
                $sales[] = $row;
            }
    
            return $sales;
        }
    }

    public function getCustIds($name){
        $sql = "SELECT id FROM customer WHERE name LIKE '$name%'";
      
        $res = $this->db->query($sql);
        $ids = [];
        while($row = $res->fetch_object()){
            $ids[] = $row->id;
        }
        return $ids;
    }

    public function searchBySeller($seller)
    {
        $ids = $this->getUserIds($seller);
        foreach($ids as $id){

            $sql = "SELECT * FROM sales WHERE user_id = '$id'";
            $res = $this->db->query($sql);

            if($res->num_rows < 1){
                throw new Exception($this->db->error);
            }

            $amcs = [];

            while( $row = $res->fetch_object() ){
                $amcs[] = $row;
            }

            return $amcs;
        }
    }

    public function getUserIds($name){
        $sql = "SELECT id FROM users WHERE name LIKE '$name%'";
        $res = $this->db->query($sql);
        $ids = [];
        while($row = $res->fetch_object()){
            $ids[] = $row->id;
        }

        return $ids;
    }

    public function getByDate($page_no = 1, $from, $to){
        $page_size = 20;
        $current_page = $page_no - 1; 

        $skip = $current_page * $page_size;

        $up_limit = $skip + $page_size;
        
        $sales = [];

        $sql = "SELECT * FROM sale WHERE DATE(date) BETWEEN DATE('$from') AND DATE('$to') LIMIT $skip , $up_limit";
        
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception($this->db->error);
        }
        
        while($row = $res->fetch_object()){
            
            $sales[] = new Sale($row);

        }
        
        return $sales;
    }

    public function findByCustID($id)
    {
        if(!$id){
            throw new Exception('Missing customer id');
        }
        $sql = "SELECT * FROM sale WHERE customer_id = '$id'";
        //echo $sql; die;
   
        $res = $this->db->query($sql);

        if($res->num_rows < 1 ){
            throw new Exception('Data not available');
        }
        $result = [];

        while($row = $res->fetch_object()){
            $result[] = new Sale($row);
        }

        return $result;
    }
 } 