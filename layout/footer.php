</div>
</div>

<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/chart.js/js/Chart.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/widget/amchart/amcharts.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/widget/amchart/serial.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/widget/amchart/light.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/SmoothScroll.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/pcoded.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/vartical-layout.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/dashboard/custom-dashboard.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/script.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/data-table/js/data-table-custom.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/data-table/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/data-table/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/pages/data-table/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?php echo PROOT ?>/resources/js/modalEffects.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

<script src="<?php echo PROOT ?>/resources/js/axios.min.js"></script>
<script src="<?php echo PROOT ?>/config/config.js"></script>
<script src="<?php echo PROOT ?>/sales/js/sale.js"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>

</html> 