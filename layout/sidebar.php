<div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">

                            <ul class="pcoded-item pcoded-left-item">
                                <li style="margin-top:15px">
                                    <a href="<?php echo PROOT?>/dashboard.php">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Dashboard</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo PROOT?>/customer/index.php">
                                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                        <span class="pcoded-mtext">Customer</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo PROOT?>/sales/index.php?page=1">
                                        <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                        <span class="pcoded-mtext">Sale</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo PROOT?>/amc/index.php">
                                        <span class="pcoded-micon"><i class="feather icon-shield"></i></span>
                                        <span class="pcoded-mtext">AMC</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
