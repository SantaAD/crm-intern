<?php
/**
 * Class User
 * Mofiqul Islam, mofi0islam@gmail.com 
 * Created at: 23-02-2019 2:36PM
 */

 class User{
    public $id = '';
    public $username = '';
    private $password = '';
    public $role = '';
    public $name = '';
    public $mobile = '';
    public $email = '';
    public $designation = '';

    private $db = '';

    public function __construct($user = null)
    {
        $this->db = Database::getInstance();

        if($user === null) return $this;
       
        $this->username = $user['username'];
        $this->password = password_hash($user['password'], PASSWORD_DEFAULT);
        $this->role = $user['role'];
        $this->name = $user['name'];
        $this->mobile = isset($user['mobile']) ? $user['mobile'] : '';
        $this->email = isset($user['email']) ? $user['email'] : '';
        $this->designation = isset($user['designation']) ? $user['designation'] : ''; 

        return $this;
    }


    /**
     * Creates a new user
     * @param ${user:object}
     * @return ${bool}
     */

     public function save()
     {

        $username = $this->username;
        $password = $this->password;
        $role = $this->role;
        $name = $this->name;
        $mobile = $this->mobile;
        $email = $this->email;
        $designation = $this->designation;

        $sql = "SELECT username FROM users WHERE username = '$this->username'";
        
        if($this->db->query($sql)->num_rows > 0) {
            throw new Exception('username already exits');
        }

        $sql = "INSERT INTO users (`username`, `password`, `role`, `name`, `mobile`, `email`, `designation`)
            VALUES ('$username', '$password', '$role', '$name', '$mobile', '$email', '$designation')";
        
        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }
        return $this->findByUsername($this->username);
     }

     public function delete($id) 
     {
         
        if(!$id) return false;

        $user = $this->find($id);

        if(!$user) return false;

        $sql = "DELETE FROM users WHERE id = '$id'";

        if($this->db->query($sql) === false){
            return false;
        }

        return $user;
     }

    public function find($id)
    {

        if(!$id) return false;

        $sql = "SELECT * FROM users WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        $user = $res->fetch_object();

        return $user;
    }

    public function findByUsername($username)
    {

        if(!$username) return false;

        $sql = "SELECT * FROM users WHERE username = '$username'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        $user = $res->fetch_object();

        return $user;
    }

    public function update($user)
    {
        foreach($user as $key => $value){
            $this->{$user[$key]} = $value;
        }
        
        $id = $this->id;
        $username = $this->username;
        $password = $this->password;
        $role = $this->role;
        $name = $this->name;
        $mobile = $this->mobile;
        $email = $this->email;
        $designation = $this->designation;

        $sql = "UPDATE users SET 
            `username` = '$username', 
            `password` = '$password', 
            `role` = '$role', 
            `name` = '$name', 
            `mobile` = '$mobile', 
            `email` = '$email', 
            `designation` = '$designation'
            WHERE id = '$id'";

        return $this->db->query($sql);
     }


 }
