<?php

class Address {

      
       
    private $db = null;

    public function __construct($data = null, Customer $customer)//address can be null
    {
        $this->db = Database::getInstance();

        if(!$data) return $this;

        foreach($data as $key => $value){
            $this->$key = $value;
        }

        $this->customer = $customer;
        
        return $this;
    }

    public function save()
    {
        $customer_id = $this->customer->id;
       
        $sql = "INSERT INTO address ( `address`, `city`, `pin`, `customer_id`)
                VALUES ( '$this->address', '$this->city', '$this->pin', '$customer_id')";
        
        if($res = $this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->db->insert_id); //this will return last id
    }
         

    public function findByID($id)
    {
        if(!$id){
            throw new Exception('Missing address id');
        }

        $sql = "SELECT * FROM address WHERE customer_id = '$id'";
        //echo $sql;
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        return $this->populateObject( $res->fetch_object() );

    } 

    private function populateObject($result)
    {
       $result = (array) $result;
       foreach($result as $key => $value){
           $this->$key = $value;
       }
       return $this;
    }


    public function update($data){

        if(!$data){
            throw new Exception('Missing fileds');
        }

        foreach($data as $key => $value){
            $this->{$data[$key]} = $value;
        }

        $sql = "UPDATE address SET  `address` = '$this->address',`pin` = '$this->pin', `city` = '$this->city' 
         WHERE customer_id = '$this->id'";
        // echo $sql; die();
        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);//

    }
//delete address
    public function delete($id){
        $sql = "DELETE FROM address WHERE customer_id = '$id'";
        //echo $sql; die();
        return ($this->db->query($sql));
    }




    }
     




?>