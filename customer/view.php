<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';
require_once '../libs/database/database.php';
require_once './Customer.php';
require_once './Address.php';

    $customer = new Customer();

    if(isset($_GET['view'])) {
       $id = $_GET['view'];
       $cus = new Customer();
       $customer = $cus->findByID($id);
       $add = new Address([], $customer);
       $address = $add->findByID($id);

     
    
         //print_r($address);die();
    }


    
    
?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                           
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Customer Details</h4>
                                        </div>
                                        <div class="card-block">
                                            <form>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Customer ID</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php  echo $customer->id; ?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Customer Name</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer->name; ?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Address</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $address->address; ?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Email</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer->email; ?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Phone</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer->phone; ?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Pin</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $address->pin; ?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">City</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $address->city; ?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <button class="btn btn-success" onclick="history.go(-1);">Back</<button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
    include '../layout/footer.php';
?>