<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';
require_once '../libs/database/database.php';
require_once './Customer.php';
require_once './Address.php';

$customer = new Customer();

if (isset($_GET['update'])) {
    $id = $_GET['update'];
    $cus = new Customer();
    $customer = $cus->findByID($id);
    $add = new Address([], $customer);
    $address = $add->findByID($id);
}

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-header">
                                    <h4>Customer Details</h4>
                                </div>
                                <div class="card-block">
                                    <form id="formUpdate">
                                        <div class="form-group row">

                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">Customer Name</label>

                                                <input type="hidden" id="id" placeholder="" value="<?php echo $customer->id ?>" />
                                                <input type="text" class="form-control" id="name" placeholder="" value="<?php echo $customer->name ?>" />

                                            </div>

                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">Address</label>
                                                <input type="text" class="form-control" id="address" placeholder="" value="<?php echo $address->address ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">Email</label>

                                                <input type="text" class="form-control" id="email" placeholder="" value="<?php echo $customer->email ?>" />
                                            </div>

                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">Phone</label>
                                                <input type="text" class="form-control" id="phone" placeholder="" value="<?php echo $customer->phone ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">Pin</label>
                                                <input type="text" class="form-control" id="pin" placeholder="" value="<?php echo $address->pin ?>" />
                                            </div>

                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">City</label>

                                                <input type="text" class="form-control" id="city" placeholder="" value="<?php echo $address->city ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label class="col-lg-4 col-form-label">GSTIN</label>
                                                <input type="text" class="form-control" id="gstin" placeholder="" value="<?php echo $customer->gstin ?>" />
                                            </div>
                                        </div>

                                      
                                        <button class="btn btn-danger btn-mat f-left" style="float:right" onclick="history.go(-1);">Cancel</button>
                                        
                                        <button type="submit" id="btn-submit" style="float:right; margin-right:10px" class="btn btn-success btn-mat f-left">Update</button>


                                </div>
                                <div class="mt-3" id="msg"></div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>




<?php include '../layout/footer.php'; ?> 

<script src="<?php echo PROOT?>/customer/js/updatecustomer.js "></script>