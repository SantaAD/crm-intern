<?php
include '../layout/header.php';
include '../layout/sidebar.php';
require_once '../libs/database/database.php';
require_once './Customer.php';
require_once './Address.php';

$cust = new Customer();

if (isset($_GET['del'])) {

    $id = $_GET['del'];
    $cus = new Customer();
    $customer = $cus->findByID($id);
    $add = new Address([], $customer);
    try {
        $add->delete($id);
        $cust = $cus->delete($id);
        echo '<script>alert("Customer deleted successfully");window.location = "./index.php"</script>';
    } catch (Exception $e) {
        echo $e->getMessage();
    }

}

if (isset($_POST['submit'])) {

    $name = $_POST['select'];
    $search_value = $_POST['search'];
    if ($name == 'name') {

        $customers = $cust->getCustomerByName($search_value);

    } else if ($name == 'phone') {

        $customers = $cust->getCustomerByPhone($search_value);

    } else if ($name == 'email') {

        $customers = $cust->getCustomerByEmail($search_value);

    }
} else {

    $customers = $cust->getAllCustomer();
}

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">


                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            
                            <div class="row">
                                        <div class="col col-lg-3">
                                             <button class="btn btn-mat btn-inverse" data-toggle="modal" data-target="#Modal">Add
                                        Customer</button>
                                        </div>
                                        <div class="col col-lg-9">
                                           <h3 style="float:right">Customer Details</h3>
                                        </div>
                                    </div>
                            
                            <div class="page-header-title">

                                <div class="d-inline">
                                    
                                   
                                    <div class="modal fade" id="Modal" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Customer Information</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <form id="formCustomer">
                                                        <div class="form-group row">
                                                            <div class="col-sm-6">
                                                                <input type="text" id="name" class="form-control" placeholder="Enter Name">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="email" id="email" class="form-control" placeholder="Enter Email">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-6">
                                                                <input type="text" id="phone" class="form-control" placeholder="Enter Phone">
                                                                <span class="messages"></span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <textarea class="form-control" id="address" placeholder="Enter Address"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-6">
                                                                <input type="text" id="pin" class="form-control" placeholder="Enter Pincode">
                                                                <span class="messages"></span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" id="city" class="form-control" placeholder="Enter City">
                                                            </div>

                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-sm-6">
                                                                <input type="text" id="gstin" class="form-control" placeholder="Enter GST no">
                                                            </div>
                                                        </div>


                                                </div>
                                                <div class="mt-3" id="msg"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-mat waves-effect " data-dismiss="modal">Close</button>
                                                    <button type="submit" id="btn-submit" class="btn btn-inverse btn-mat waves-effect waves-light ">Submit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Filter options</h5>
                                    <form action="" method="post">

                                        <div class="form-group row">
                                            <div class="col col-sm-12 col-md-2">
                                                <select name="select" id="" class="form-control sm">
                                                    <option>SELECT</option>
                                                    <option value="name">Name</option>
                                                    <option value="phone">Phone</option>
                                                    <option value="email">Email</option>

                                                </select>
                                            </div>

                                            <div class="col col-sm-12 col-md-4">
                                                <input type="text" name="search" class="form-control sm" placeholder="Search">
                                            </div>



                                            <div class="col col-sm-12 col-md-2">
                                                <button type="submit" name="submit" class="btn btn-success btn-sm btn-mat" style="font-size:18px;"><i class="feather icon-filter"></i></button>
                                            </div>
                                            

                                        </div>
                                    </form>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="simpletable" class="table table-hover table-bordered nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Sl. No.</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>

                                                    <th>Pin</th>
                                                    <th>City</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($customers as $customer) : ?>
                                                <tr>
                                                    <td><?php echo $customer->id; ?></td>
                                                    <td><a class="text-info" href="<?php echo PROOT ?>/customer/profile.php?profileview=<?php echo $customer->id ?>"><?php echo $customer->name; ?></td>
                                                    <td><?php echo $customer->email; ?></td>
                                                    <td><?php echo $customer->phone; ?></td>
                                                    <?php $add = $customer->getAddress() ?>
                                                    <td><?php echo $add->pin ?></td>
                                                    <td><?php echo $add->city ?></td>
                                                    <td>
                                                        <div class="dropdown-inverse dropdown open">
                                                            <button class="btn btn-mat btn-inverse dropdown-toggle waves-effect waves-light " type="button" id="dropdown-7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdown-7" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                                <a class="dropdown-item waves-light waves-effect" href="<?php echo PROOT ?>/customer/profile.php?profileview=<?php echo $customer->id ?>">
                                                                    <i class="text-success feather icon-eye"></i>
                                                                    View
                                                                </a>
                                                                <a class="dropdown-item waves-light waves-effect" onclick="return confirm('Are you sure to delete')" href="?del=<?php echo $customer->id; ?> ">
                                                                    <i class="text-danger feather icon-trash-2"></i>
                                                                    Delete
                                                                </a>
                                                                <a class="dropdown-item waves-light waves-effect" href="<?php echo PROOT ?>/customer/update.php?update=<?php echo $customer->id ?>">
                                                                    <i class="text-success feather icon-edit "></i>
                                                                    Update</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../layout/footer.php';?> 
<script src="<?php echo PROOT?>/customer/js/customer.js "></script>