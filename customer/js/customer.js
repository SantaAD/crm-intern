const formCustomer = document.getElementById('formCustomer');
formCustomer.addEventListener('submit', function (e) {
   e.preventDefault();
   let name = document.getElementById('name');
   let email = document.getElementById('email');
   let phone = document.getElementById('phone');
   let address = document.getElementById('address');
   let pin = document.getElementById('pin');
   let city = document.getElementById('city');
   let gstin = document.getElementById('gstin');
   let msgArea = document.getElementById('msg');
   let submitBtn = document.getElementById('btn-submit');

   var params = new URLSearchParams(); //
   params.append('name', name.value);
   params.append('email', email.value);
   params.append('phone', phone.value);
   params.append('address', address.value);
   params.append('pin', pin.value);
   params.append('city', city.value);
   params.append('gstin', gstin.value);

   axios.post(config.host + '/customer/customer-insert.php', params)
      .then(function (response) {

         if (response.data.status == 'success') {
            alert('Customer added successfully');
            location.reload();
         } else {
            alert('Failed to add customer');
         }

      })
      .catch(function (error) {
         submitBtn.innerHTML = 'submit';
         console.log(error);
      });
})