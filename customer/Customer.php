
<?php 


/**
 * Class Customer
 * Syed Injamul Haque, injamul.haque6@gmail.com 
 * Created at: 24-02-2019 8:39PM
 */
    
   class Customer {

    private $db = null;

    public function __construct( $data = null)
    {
        $this->db = Database::getInstance();

        if(!$data) return $this;

        foreach($data as $key => $value){
            $this->$key = $value;
        }
        
        return $this;
    }

    public function save()
    {
        $this->id = $this->generateID();
        $sql = "INSERT INTO customer (`id`, `email`, `name`, `phone`, `gstin`)
                VALUES
                ('$this->id', '$this->email', '$this->name', '$this->phone', '$this->gstin') ";
       // echo $sql; die();
        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);
    }

    public function findByID($id)
    {
        if(!$id){
            throw new Exception('Missing customer id');
        }

        $sql = "SELECT * FROM customer WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        return $this->populateObject( $res->fetch_object() );

    } 

    private function generateID(){
        $sql = "SELECT max(id) as max_id FROM customer";
        $res = $this->db->query($sql);
        $maxID = $res->fetch_object()->max_id;
        if($maxID == 0){
            return $maxID + 100000;
        }

        return $maxID + 1;
    }

//pagination
    public function get($page_no = 1){
        $page_size = 20;
        $current_page = $page_no - 1; 

        $skip = $current_page * $page_size;

        $up_limit = $skip + $page_size;

        $sql = "SELECT * FROM customer LIMIT $skip , $up_limit";
        
        $customers = [];

        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception($this->db->error);
        }
        
        while($row = $res->fetch_object()){
            
            $customers[] = new Customer($row);

        }
        
        return $customers;
    }

    public function update($data){

        if(!$data){
            throw new Exception('Missing fileds');
        }

        foreach($data as $key => $value){
            $this->{$data[$key]} = $value;
        }

        $sql = "UPDATE customer SET  `email` = '$this->email', 
        `name` = '$this->name', `phone` = '$this->phone', `gstin` = '$this->gstin'
         WHERE id = '$this->id'";
         //echo $sql; die();

        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);

    }

    private function populateObject($result)
    {
       $result = (array) $result;
       foreach($result as $key => $value){
           $this->$key = $value;
       }
       return $this;
    }

    public function getAllCustomer() 
    {

       $sql = "SELECT * FROM customer";
       $res = $this->db->query($sql);

       if($res->num_rows < 1) return false;

       $cus = [];
       while($row = $res->fetch_object()) {
           $cus[] = new Customer((array)$row); //returning customer obj
       }

     return ($cus);


    }

    //delet customer
    public function delete($id){
        $sql = "DELETE FROM customer WHERE id = '$id'";
        //echo $sql;
       // print_r($sql);die();
        return ($this->db->query($sql));
    }

    //get address of customer

    public function getAddress(){

        $sql = "SELECT * FROM address WHERE customer_id = '$this->id'";
        //print_r($sql); die();
        //return $sql;
        $res = $this->db->query($sql);
        
        return $res->fetch_object();
   }

   public function getCustomerByName($name)
   {
       if(!$name) {
           throw new Exception(' Name is Missing ');
       }

       $sql = "SELECT * FROM customer WHERE name LIKE '%$name%' ";
       //echo $sql; die();

       $res = $this->db->query($sql);

       if($res->num_rows < 1) {
           throw new Exception('Data not available');
       }
      $customers = [];
      while($row = $res->fetch_object()) {
         $customers[] = new Customer($row);
      }
      return $customers;
     

   }
   public function getCustomerByPhone($phone)
   {
       if(!$phone) {
           throw new Exception('Phone is Missing');
       }

       $sql = "SELECT * FROM customer WHERE phone = '$phone'";
      // echo $sql;die();
       $res = $this->db->query($sql);
       if($res->num_rows < 1) {
           throw new Exception("Data not Available");
       }
       $customers = [];
       while($row = $res->fetch_object()) {
           $customers[] = new Customer($row);
       }
       return $customers;
   }

   //search by email
   public function getCustomerByEmail($email)
   {
       if(!$email) {
           throw new Exception('Email is Missing');
       }

       $sql = "SELECT * FROM customer WHERE email = '$email'";
       //echo $sql;die();
       $res = $this->db->query($sql);
       if($res->num_rows < 1) {
           throw new Exception("Data not Available");
       }
       $customers = [];
       while($row = $res->fetch_object()) {
           $customers[] = new Customer($row);
       }
       return $customers;
   }

  public function getCustomerByAddress($address)
  {
      if(!$address)
      {
            throw new Exception ('Address is missing');
      }
      $sql = "SELECT * FROM address WHERE address LIKE '%$address%' ";
      //echo $sql; die();
      $res = $this->db->query($sql);
      if($res->num_rows < 1) {
          throw new Exception("Data not available");
      }

      $customers = [];
      while($row = $res->fetch_object()) {
          $customers[] = new Customer($row);
      }
      return $customers;
  }
  

  
  

   }




?>