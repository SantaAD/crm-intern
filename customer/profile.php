<?php

include '../layout/header.php';
include '../layout/sidebar.php';
require_once '../libs/database/database.php';
require_once './Customer.php';
require_once './Address.php';
require_once '../amc/interfaces/AmcInterface.php';
require_once '../amc/Model/Amc.php';
require_once '../sales/interfaces/interface_sale.php';
require_once '../sales/classes/sale.php';

$customer = new Customer();

if (isset($_GET['profileview'])) {
    $id = $_GET['profileview'];
    $cus = new Customer();
    $customer = $cus->findByID($id);

    $add = new Address([], $customer);
    $address = $add->findByID($id);

    try {
        $sale = new Sale();
        $sales = $sale->findByCustID($id);
    } catch (Exception $e) {
        $sales = false;
    }
    
    try {

        $amc = new Amc();

        $amcDetails = $amc->getAmcByCusId($id);

    } catch (Exception $e) {
        $amcDetails = false;
    }
}

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-block tab-icon">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xl-12">

                                    <div class="sub-title">Customer Information</div>

                                    <ul class="nav nav-tabs md-tabs justify-content-around" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab"><i
                                                    class="icofont icofont-home"></i>Personal Information</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#sales" role="tab"><i class="icofont icofont-ui-user "></i>Sales</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#amc" role="tab"><i class="icofont icofont-ui-message"></i>AMC</a>
                                            <div class="slide"></div>
                                        </li>
                                    </ul>

                                    <div class="tab-content card-block">
                                        <div class="tab-pane active" id="personal" role="tabpanel">
                                                <div class="card-header">
                                                    <h4>Basic Details</h4>
                                                </div>
                                                <div class="card-block">
                                                    <form>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Name</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $customer->name; ?>
                                                                </strong></label>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Email</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $customer->email; ?></strong></label>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Phone</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $customer->phone; ?></strong></label>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">GST Number</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $customer->gstin; ?></strong></label>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="card-header">
                                                    <h4>Address Details</h4>
                                                </div>
                                                <div class="card-block">
                                                    <form>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Address</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $address->address; ?>
                                                                </strong></label>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">Pin</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $address->pin; ?></strong></label>

                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-2 col-form-label">City</label>
                                                            <label class="col-lg-6 col-form-label"><strong>
                                                                    <?php echo $address->city; ?></strong></label>
                                                        </div>
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="tab-pane" id="sales" role="tabpanel">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive table-sm">
                                                    <table id="simpletable" class="table table-bordered nowrap table-hover">
                                                        <thead class="center">
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Total</th>
                                                                <th>Sub-Total</th>
                                                                <th>GST Total</th>
                                                                <th>Date</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($sales) : ?>
                                                            <?php foreach ($sales as $sale) : ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $sale->id ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $sale->total ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $sale->sub_total ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $sale->gst_total ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $sale->date ?>
                                                                </td>
                                                                
                                                                <td>
                                                                    <div class="dropdown-primary dropdown open">
                                                                        <button class="btn btn-primary dropdown-toggle waves-effect waves-light "
                                                                            type="button" id="dropdown-7" data-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="true">Action</button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdown-7"
                                                                            data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                                            <a class="dropdown-item waves-light waves-effect"
                                                                                href="<?php echo PROOT ?>/sales/view.php?sale=<?php echo $sale->id ?>">View</a>


                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            <?php endforeach;
                                                    endif ?>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="amc" role="tabpanel">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive table-sm">

                                                    <table id="simpletable" class="table table-bordered nowrap table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Amount</th>
                                                                <th>Start date</th>
                                                                <th>End date</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if($amcDetails):?>
                                                            <?php foreach ($amcDetails as $amc) : ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $amc->id ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $amc->amount ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $amc->start_date ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo $amc->end_date ?>
                                                                </td>
                                                               
                                                                <td>
                                                                    <div class="dropdown-inverse dropdown open">
                                                                        <button class="btn btn-mat btn-inverse dropdown-toggle waves-effect waves-light "
                                                                            type="button" id="dropdown-7" data-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="true">Action</button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdown-7"
                                                                            data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                                            <a class="dropdown-item waves-light waves-effect"
                                                                                href="<?php echo PROOT ?>/amc/amcview.php?view=<?php echo $amc->id ?>">View</a>

                                                                        </div>
                                                                    </div>
                                                </div>
                                                </td>
                                                </tr>
                                                <?php endforeach ?>
                                                <?php endif?>



                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include '../layout/footer.php';
?>