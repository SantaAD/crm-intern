<?php

  ini_set('display_errors', 1);
  
  if(!isset($_REQUEST['name']) || empty($_REQUEST['name'])) {
    
    $array = array(
        "msg" => "please enter the name",
        "status" => "failed" 
    );
    echo json_encode($array);
    return;
  }
   if(!isset($_REQUEST['email']) || empty($_REQUEST['email'])) {
    $array = array(
        "msg" => "please enter the Email",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }

   if(!isset($_REQUEST['phone']) || empty($_REQUEST['phone'])) {
    $array = array(
        "msg" => "please enter the Phone number",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }

   if(!isset($_REQUEST['address']) || empty($_REQUEST['address'])) {
    $array = array(
        "msg" => "please enter the Address",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }

   if(!isset($_REQUEST['pin']) || empty($_REQUEST['pin'])) {
    $array = array(
        "msg" => "please enter the pin",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }

   if(!isset($_REQUEST['city']) || empty($_REQUEST['city'])) {
    $array = array(
        "msg" => "please enter the City",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }

   if(!isset($_REQUEST['gstin']) || empty($_REQUEST['gstin'])) {
    $array = array(
        "msg" => "please enter the gst no",
        "status" => "failed"
    );
    echo json_encode($array);
    return;
   }


    require_once '../libs/database/database.php';
    require_once './Customer.php';
    require_once './Address.php';

   $customer = new Customer($_REQUEST);
   $s = $customer->save();
  //print_r($s); die();
   $address = new Address($_REQUEST, $s);
   $res = $address->save();
   echo json_encode(['status'=> 'success', 'msg' => 'Customer added']);






  



  ?> 