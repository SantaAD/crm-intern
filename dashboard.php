<?php
include './layout/header.php';
include './layout/sidebar.php';
require_once './libs/database/database.php';
$db = Database::getInstance();

$sql = "select count(id) as total_cust from customer";
$res = $db->query($sql);
$c = $res->fetch_object()->total_cust;

$sql = "select count(id) as total_sale from sale";
$res = $db->query($sql);
$s = $res->fetch_object()->total_sale;

$sql = "select count(id) as total_amc from amc";
$res = $db->query($sql);
$a = $res->fetch_object()->total_amc;

$sql = "select count(id) as total_item from items";
$res = $db->query($sql);
$i = $res->fetch_object()->total_item;


?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">


                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-body">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-yellow update-card">
                                <div class="card-block">
                                    <div class="row align-items-end">
                                        <div class="col-8">
                                            <h4 class="text-white"><?php echo $c?></h4>
                                            <h6 class="text-white m-b-0">Customers</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <canvas id="update-chart-1" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="text-white m-b-0"><i class="feather icon-eye"></i> <a class="text-default" href="<?php echo PROOT?>/customer/index.php">View details</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-green update-card">
                                <div class="card-block">
                                    <div class="row align-items-end">
                                        <div class="col-8">
                                            <h4 class="text-white"><?php echo $s?>+</h4>
                                            <h6 class="text-white m-b-0">Sales</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <canvas id="update-chart-2" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="text-white m-b-0"><i class="feather icon-eye"></i> <a class="text-default" href="<?php echo PROOT?>/sales/index.php">View details</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-pink update-card">
                                <div class="card-block">
                                    <div class="row align-items-end">
                                        <div class="col-8">
                                            <h4 class="text-white"><?php echo $a?></h4>
                                            <h6 class="text-white m-b-0">AMC</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <canvas id="update-chart-3" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="text-white m-b-0"><i class="feather icon-eye"></i> <a class="text-default" href="<?php echo PROOT?>/amc/index.php">View details</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-c-lite-green update-card">
                                <div class="card-block">
                                    <div class="row align-items-end">
                                        <div class="col-8">
                                            <h4 class="text-white"><?php echo $i?></h4>
                                            <h6 class="text-white m-b-0">Items</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <canvas id="update-chart-4" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <p class="text-white m-b-0"><i class="feather icon-eye"></i> <a class="text-default" href="#">View details</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
include './layout/footer.php';
?> 