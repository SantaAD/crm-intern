<?php
/**
 * Class Session
 * Mofiqul Islam, mofi0islam@gmil.com 
 * 
 * Created at: 23-02-2019 02:11PM
 */


 class Session{
     public function __construct()
     {
         session_start();
     }

     /**
      * Sets a new session variable with key value pair
      * @param ${key:mixed) $${value:mixed}
      * @return ${bool}
      */
     public static  function set($key, $value): bool {
        
        if(!$key || !$value) return false;

        return $_SESSION[$key] = $value;
     }

     /**
      * Returns a session value
      * @param ${key:String}
      * @return ${String}
      */
     public static function get($key): string {

        if(!isset($_SESSION[$key])) return false;

        return $_SESSION[$key];
     }

     /**
      * Deletes a session 
      * @param $(key:String)
      * @return ${1:bool}
      */

     public static function delete($key): bool {
        
        if(!isset($_SESSION[$key])) return false;

        unset( $_SESSION[$key] );
        return true;
     }
 }

