<?php

class Database{

    private static $instance = null;

    /**
     * @return object $instance
     */
    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new mysqli('localhost', 'root', 'password', 'crm');
        }            
        return self::$instance;
    }

    
    
}

?>
