<?php


class AmcProduct
{
    private $db = null;

    public function __construct($data = null)
    {
        $this->db = Database::getInstance();

        if(!$data) return $this;

        foreach($data as $key => $value){
            $this->$key = $value;
        }
        //$this->amc = $amc;
        
        return $this;
    }

    public function save()
    {
        //$amc = new AMC();
        //$id = $amc->insert_id;
       
        $sql = "INSERT INTO amc_product ( `name`, `model`, `serial`, `price`,`quantity`, `purchase_date`, `warranty`, `manufactured_by`, `barcode`,`amc_id`)
                VALUES ( '$this->name', '$this->model', '$this->serial', '$$this->price','$this->quantity', '$this->purchase_date', '$this->warranty', '$this->manufactured_by', '$this->barcode','$this->amc_id')";
        
        if($res = $this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->db->insert_id); //this will return last id
    }
         

    public function findByID($id)
    {
        if(!$id){
            throw new Exception('Missing address id');
        }

        $sql = "SELECT * FROM amc_product WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        return $this->populateObject( $res->fetch_object() );

    } 

    private function populateObject($result)
    {
       $result = (array) $result;
       foreach($result as $key => $value){
           $this->$key = $value;
       }
       return $this;
    }


    public function update($data){

        if(!$data){
            throw new Exception('Missing fileds');
        }

        foreach($data as $key => $value){
            $this->{$data[$key]} = $value;
        }

        $sql = "UPDATE amc_product SET  `name` = '$this->address', `model` = '$this->model',
        `serial` = '$this->serial', `price` = '$this->price', `purchase_date` = '$this->purchase_date',
        `warranty` = '$this->warranty', `manufactured_by` = '$this->manufactured_by',
        `barcode` = '$this->barcode'
         WHERE id = '$this->id'";

        if($this->$db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);//

    }

    public function getProductDetails($id)
    {
        $sql = "SELECT * FROM amc_product WHERE amc_id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) {
            throw new Exception('Data not available');
        }

        $report = [];
        while ($row = $res->fetch_object()){
            //return $this->populateObject( $res->fetch_object() );
            $report[] = new AmcProduct($row);
        }

        return $report;
    }
}

?>