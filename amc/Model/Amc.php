<?php
/**
 * Class AMC 
 * Mofiqul Islam, mofi0islam@gmail.com 
 * Created at : 24-02-2019 8:59PM
 */

 class AMC implements interfaceAMC
 {

    public $status = 'Active';

    private $db = null;

    /**
     * Creates a new AMC 
     * @param ${data:array}
     * @return ${AMC:AMC} 
     */
    //array $data = []
    public function __construct($data = null)
    {
        $this->db = Database::getInstance();

        if(!$data) return $this;

        foreach($data as $key => $value){
            $this->$key = $value;
        }
        
        return $this;
    }

    public function save()
    {
        $this->id = $this->generateID();
        $sql = "INSERT INTO amc (`id`, `start_date`, `end_date`, `amount`, `customer_id`, `sale_id`)
                VALUES
                ('$this->id', '$this->start_date', '$this->end_date', '$this->amount', '$this->customer_id', '$this->sale_id')
        ";
        
        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);
    }

    public function findByID($id)
    {
        if(!$id){
            throw new Exception('Missing amc id');
        }

        $sql = "SELECT * FROM amc WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) return false;

        return $this->populateObject( $res->fetch_object() );

    } 


    private function generateID(){
        $sql = "SELECT max(id) as max_id FROM amc";
        $res = $this->db->query($sql);
        $maxID = $res->fetch_object()->max_id;
        if($maxID == 0){
            return $maxID + 100000;
        }

        return $maxID + 1;
    }

    public function update($data){

        if(!$data){
            throw new Exception('Missing fileds');
        }

        foreach($data as $key => $value){
            $this->$key = $value;
        }

        $sql = "UPDATE amc SET  `start_date` = '$this->start_date', 
        `end_date` = '$this->end_date', `amount` = '$this->amount' 
        WHERE id = '$this->id'";

        if($this->db->query($sql) === false){
            throw new Exception($this->db->error);
        }

        return $this->findByID($this->id);

    }


    private function populateObject($result)
    {
       $result = (array) $result;
       foreach($result as $key => $value){
           $this->$key = $value;
       }
       return $this;
    }

    public function delete(){
        $sql = "DELETE FROM amc WHERE id = '$this->id'";
        return ($this->db->query($sql));
    }

    public function findByModelSerial($name)
    {
        if (!$name){
            throw new Exception('Invalid Input..');
        }

        $sql = "SELECT amc_id FROM amc_product WHERE model = '$name' OR serial = '$name'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Cannot find data.');
        }

        $amc_id = [];
        while($row = $res->fetch_object()){
            $amc_id[] = $row->amc_id;
        }

        return $amc_id;
    }

    public function getAmcById($id)
    {
        if(!$id){
            throw new Exception('Invalid Id Provided.');
        }

        $sql = "SELECT * FROM amc WHERE id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Data not available');
        }

        $result = [];
        while($row = $res->fetch_object()){
            $result[] = new AMC($row);
        }

        return $result;
    }

    public function getAmc($data)
    {
        if (!$data){
            throw new Exception('Invalid Parameter');
        }

        $amc_id = $this->findByModelSerial($data);

        $result = [];
        foreach($amc_id as $id){
            $result[] = $this->getAmcById($id);
        }

        return $result;   
    }

    public function getAmcByCusId($id)
    {
        if(!$id){
            throw new Exception('Invalid Id Provided.');
        }

        $sql = "SELECT * FROM amc WHERE customer_id = '$id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Data not available');
        }

        $result = [];
        while($row = $res->fetch_object()){
            $result[] = new AMC($row);
        }

        return $result;
    }

    public function findCusId($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }

        $sql = "SELECT id FROM customer WHERE name LIKE '%$name%'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Invalid Input.');
        }

        $customer_id = [];
        while($row = $res->fetch_object()){
            $customer_id[] = $row->id;
        }

        return $customer_id ;
    }

    public function getAmcByName($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }
        $customer_id = $this->findCusId($name);

        $get_amc = [];
        foreach($customer_id as $cus_id){
            $get_amc[] = $this->getAmcByCusId($cus_id);
        }

        return $get_amc;

    }

    public function findCusIdByPh($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }

        $sql = "SELECT id FROM customer WHERE phone = '$name'";
        //echo $sql;
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Invalid Input.');
        }

        $customer_id = [];
        while($row = $res->fetch_object()){
            $customer_id[] = $row->id;
        }

        return $customer_id ;
    }

    public function getAmcByPh($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }
        $customer_id = $this->findCusIdByPh($name);

        $get_amc = [];
        foreach($customer_id as $cus_id){
            $get_amc[] = $this->getAmcByCusId($cus_id);
        }

        return $get_amc;

    }

    public function findCusIdByAdd($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }

        $sql = "SELECT customer_id FROM address WHERE address LIKE '%$name%'";
        //echo $sql;
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Invalid Input.');
        }

        $customer_id = [];
        while($row = $res->fetch_object()){
            $customer_id[] = $row->customer_id;
        }

        return $customer_id ;
    }

    public function getAmcByAdd($name)
    {
        if (!$name){
            throw new Exception('Invalid Parameter Passed');
        }
        $customer_id = $this->findCusIdByAdd($name);

        $get_amc = [];
        foreach($customer_id as $cus_id){
            $get_amc[] = $this->getAmcByCusId($cus_id);
        }

        return $get_amc;

    }

    public function amcReportAll()
    {
        $sql = "SELECT * FROM amc";
        $res = $this->db->query($sql);

        if($res->num_rows < 1) {
            throw new Exception('Data not available');
        }

        $report = [];
        while ($row = $res->fetch_object()){
            //return $this->populateObject( $res->fetch_object() );
            $report[] = new AMC($row);
        }

        return $report;

    }

    public function amcReport($from, $to)
    {
        if (!$from || !$to){
            throw new Exception('Missing required paraameter');
        }

        $sql = "SELECT * FROM amc WHERE start_date >= '$from' AND start_date <= '$to' ORDER BY id ASC";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Invalid serach option');
        }

        $result = [];
        while ($row = $res->fetch_object()){
            $result[] = new AMC($row);
        }

        return $result;
    }
                                                                                                              
    public function checkExpiry()
    {

        $sql = "SELECT * FROM amc WHERE end_date = DATE_ADD(CURDATE(), INTERVAL 8 DAY)";
        //echo $sql;
        $res = $this->db->query($sql);
        //echo $res;

        if($res->num_rows < 1) return false;

        $date_result = [];
        while ($row = $res->fetch_object()){
            $date_result[] = $this->populateObject($row);
        }

        return $date_result;
    }
    
    public function findCustName()
    {
        $sql = "SELECT name FROM customer WHERE id = '$this->customer_id'";
        $res = $this->db->query($sql);


        if($res->num_rows < 1) {
            throw new Exception('Data could not found');
        }

        return $res->fetch_object()->name ;
    }

    public function getCustDetails()
    {
        $sql = "SELECT * FROM customer WHERE id = '$this->customer_id'";
        $res = $this->db->query($sql);
        if($res->num_rows < 1) {
            throw new Exception('Data could not found');
        }

        return new Customer( $res->fetch_object() );
    }

    public function getAddress()
    {
        $sql = "SELECT address FROM address WHERE customer_id = '$this->customer_id'";
        $res = $this->db->query($sql);


        if($res->num_rows < 1) {
            throw new Exception('Data could not found');
        }

        return $res->fetch_object()->address ;
    }

    public function getAmcProdDetails()
    {
        $amc_product =  new AmcProduct( );
        return $amc_product->getProductDetails($this->id);
    }

    public function getItemsDetails()
    {
        $amc_product =  new sale( );
        return $amc_product->getItemsDetails();
    }

    public function getItemId($sale_id)
    {
        if (!$sale_id){
            throw new Exception('Invalid Parameter Passed');
        }

        $sql = "SELECT item_id FROM sale_items WHERE sale_id = '$sale_id'";
        //echo $sql;
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Invalid Input.');
        }

        $item_id = [];
        while($row = $res->fetch_object()){
            $item_id[] = $row->item_id;
        }

        return $item_id ;
    }

    public function getItem($item_id)
    {
        if (!$item_id){
            throw new Exception('Invalid Parameter Passed');
        }

        $sql = "SELECT * FROM items WHERE id = '$item_id'";
        $res = $this->db->query($sql);

        if($res->num_rows < 1){
            throw new Exception('Dta missing');
        }

        $get_items = [];
        while($row = $res->fetch_object()){
            $get_items[] = new Item($row);
        }

        return $get_items;
    }

    public function getItems($sale_id)
    {
        if (!$sale_id){
            throw new Exception('Invalid Parameter Passed');
        }
        $item_id = $this->getItemId($sale_id);

        $get_item = [];
        foreach($item_id as $items){
            $get_item[] = $this->getItem($items);
        }

        return $get_item;

    }

 }