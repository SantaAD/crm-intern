<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';

require_once '../libs/database/database.php';
require_once './interfaces/AmcInterface.php';
require_once './Model/Amc.php';
require_once './add-amc.php';
require_once '../sales/interfaces/interface_sale.php';
require_once '../sales/classes/sale.php';

$amc = new Sale();

try{
    $amc = $amc->get();
}catch(Exception $e){
    echo 'Error Message: '.$e->getMessage();
}

?>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">

                    
                        
                        <div class="col col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3>AMC Information</h3>
                                </div>
                                <form action="<?php echo $_SERVER['PHP_SELF']?>" method="POST" id="add_amc">
                                        <div class="card-block">
                                        <div class="form-group row">
                                            <label for="search-customer"><h4>Search customer</h4></label>
                                            <input type="text" list="customers" id="search-customer" class="form-control" placeholder="Serach customer">
                                            <!--<table id="customers" class="table table-hover"></table>-->
                                        </div>
                                        <div class="form-group row">
                                            <select name="customer" style="font-size:20px; padding:10px 10px" id="customers" multiple class="form-control">
                                                
                                            </select>
                                            <!--<table id="customers" class="table table-hover"></table>-->
                                        </div>
                                        
                                        </div>
                                    
                                    <div class="card-block">
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <select name="sale_id" class="form-control sm"
                                                onchange="java_script_:show(this.options[this.selectedIndex].value)">
                                                    <option value="">Select Sale Id</option>
                                                    <option value="zero">None</option>
                                                    <?php foreach($amc as $amci):?>
                                                    <option value="<?php echo $amci->id?>"><?php echo $amci->id?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="amount" class="form-control autonumber" placeholder="Price">
                                            </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <input type="text" name="start_date" class="form-control date" data-mask="99/99/9999"
                                                    placeholder="Start date">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="end_date" class="form-control date" data-mask="99/99/9999"
                                                    placeholder="End date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-block" id="hiddenDiv" style="display:none">
                                    <h3>Product Details</h3>
                                    <br>
                                        <div class="form-group" id="clone">
                                        <h5>Item</h5>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <input type="text" name="product_name[]" class="form-control" placeholder="Product Name">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="model[]" class="form-control" placeholder="Model"> 
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="serial[]" class="form-control" placeholder="Serial"> 
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="quantity[]" class="form-control" placeholder="Quantity">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <input type="text" name="product_price[]" class="form-control autonumber" placeholder="Product Price">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="manufactured_by[]" class="form-control" placeholder="Manufactured By">
                                            </div>               
                                            <div class="col-sm-3">
                                                <input type="text" name="purchased_date[]" class="form-control date" data-mask="99/99/9999"
                                                placeholder="Purchased Date">
                                            </div> 
                                            <div class="col-sm-3">
                                                <input type="text" name="warranty[]" class="form-control" placeholder="Warranty">
                                            </div>               
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <input type="text" name="barcode[]" class="form-control" placeholder="Barcode">
                                            </div>               
                                        </div>
                                        </div>

                                        <div id="add-cloned"></div>

                                        <div class="col-sm-12">
                                            <a class="btn btn-success" id="clone-btn"><i class="feather icon-plus"></i></a>
                                        </div>
                                    </div>
                                    
                                    
                                <div class="row text-center">
                                    <div class="col-sm-12 invoice-btn-group text-center" style="margin-bottom:10px; text-align:right">
                                        <button type="submit" name="submit" class="btn btn-primary btn-mat">Submit</button>
                                        <button type="button" class="btn btn-danger btn-mat">Cancel</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php
include '../layout/footer.php';
?>


<script>
    let searchCustomer = document.getElementById('search-customer');

    let dataList = document.getElementById('customers');

    searchCustomer.addEventListener('keyup', function(event) {
        let value = event.target.value;
        let input = new URLSearchParams();
        input.append('input', value);
        dataList.innerHTML = '';
        axios.post('http://localhost/crm/sales/get-customer-list.php', input)
            .then(response => {

                console.log(response.data.customers);
                for (let i = 0; i < response.data.customers.length; i++) {
                    


                    let option = `<option value="${response.data.customers[i].id}"> ${response.data.customers[i].id}, ${response.data.customers[i].name}</option>`;


                    dataList.innerHTML += option;
                }

            })
            .catch(error => {
                console.log(error);
            })
    })

    dataList.addEventListener('change', function (event) {
    let customerId = event.target.value;

    let params = new URLSearchParams();
    params.append('id', customerId);

    axios.post('http://localhost/crm/sales/get-customer-details.php', params)
        .then(response => {
            console.log(response.data.customer);
            document.getElementById('client-id').innerHTML = response.data.customer.id;
            document.getElementById('client-name').innerHTML = response.data.customer.name;

        })
        .catch(error => {
            console.log(error);
        })

});
</script>

<script>
    $('#clone-btn').click(function(){
        $('#clone').clone(true).removeAttr('id').appendTo('#add-cloned');
    });
</script>

<script>
    function show(selectedIndex) {
        if (selectedIndex != "zero") {
            hiddenDiv.style.display='none';
            
        }else{
            hiddenDiv.style.display='inline-block';
            Form.fileURL.focus();
        }
    }
</script>