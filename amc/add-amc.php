<?php

require_once '../libs/database/database.php';
require_once './interfaces/AmcInterface.php';
require_once './Model/Amc.php';
require_once './Model/AmcProduct.php';

if(isset($_POST['submit'])){
    $sale_id = $_POST['sale_id'];

if ($sale_id != 'zero') {
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $amount =  $_POST['amount'];
    $customer_id = $_POST['customer'];

    try{
        $amc = new AMC([
            'sale_id' => $sale_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'amount' => $amount,
            'customer_id' => $customer_id
        ]);
        $amc = $amc->save();
        echo "<script>alert('AMC Successfully added');</script>";
    } catch(Exception $e){
       
        echo "<script>alert('Failed to add AMC');</script>";
    }

} else {
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $amount =  $_POST['amount'];
    $customer_id = $_POST['customer'];
    $product_name = $_POST['product_name'];
    $model = $_POST['model'];
    $serial = $_POST['serial'];
    $quantity = $_POST['quantity'];
    $product_price = $_POST['product_price'];
    $warranty = $_POST['warranty'];
    $manufactured_by = $_POST['manufactured_by'];
    $purchased_date = $_POST['purchased_date'];
    $barcode = $_POST['barcode'];

    try{
        $amc = new AMC([
            'sale_id' => $sale_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'amount' => $amount,
            'customer_id' => $customer_id
        ]);
        $amc = $amc->save();
        //var_dump($)
    
        foreach ($product_name as $key => $value) {
            $product_nameas = $product_name[$key];
            $modelas = $model[$key];
            $serialas = $serial[$key];
            $quantityas = $quantity[$key];
            $product_priceas = $product_price[$key];
            $warrantyas = $warranty[$key];
            $manufactured_byas = $manufactured_by[$key];
            $purchased_dateas = $purchased_date[$key];
            $barcodeas = $barcode[$key];
    
            $amc_prod = new AmcProduct([
                'name' => $product_nameas,
                'model' => $modelas,
                'serial' => $serialas,
                'price' => $product_priceas,
                'quantity' => $quantityas,
                'purchase_date' => $purchased_dateas,
                'warranty' => $warrantyas,
                'manufactured_by' => $manufactured_byas,
                'barcode' => $barcodeas,
                'amc_id' => $amc->id
            ]);
            //print_r($amc_prod);die;
            $amcp = $amc_prod->save();
            }
            echo "<script>alert('AMC Successfully added');</script>";
    } catch(Exception $e){
        echo "<script>alert('Failed to add AMC');</script>";
    }

}
}
 