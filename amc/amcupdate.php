<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';

require_once '../libs/database/database.php';
require_once './interfaces/AmcInterface.php';
require_once './Model/Amc.php';

$amc = new AMC();

if ($_GET['update']) {
    $id = $_GET['update'];
    try{
        $amc = $amc->findById($id);
        echo '<script>alert("AMC updated successfully); window.location = "./index.php"</script>';
    }catch(Exception $e){
        echo '<script>alert("AMC not updated); window.location = "./index.php"</script>';
    }
}
?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div id="msg"></div>
                    <div class="row">
                        <div class="col-sm-8 offset-2">

                            <div class="card" style="text-align:center">
                                <div class="card-header">
                                    <h4>AMC Details</h4>
                                </div>
                                <div class="card-block">
                                    <form id="update">
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Start Date</label>
                                            <div class="col-sm-5">
                                                <input type="hidden" id="id" value="<?php echo $id ?>">
                                                <input type="text" value="<?php echo $amc->start_date ?>" class="form-control date" data-mask="99/99/9999" id="start_date" placeholder="dd/mm/yyyy">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">End Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" value="<?php echo $amc->end_date ?>" class="form-control date" data-mask="99/99/9999" id="end_date" placeholder="dd/mm/yyyy">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Price</label>
                                            <div class="col-sm-5">
                                                <input type="text" value="<?php echo $amc->amount ?>" class="form-control" id="amount" placeholder="Amount">
                                            </div>
                                        </div>
                                        <div class="row text-center">
                                            <div class="col-sm-12 invoice-btn-group text-center" style="margin-bottom:10px;">
                                                <button type="submit" id="submit" class="btn btn-primary btn-mat">Update</button>
                                                <button type="button" onclick="history.go(-1)" class="btn btn-danger btn-mat">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include '../layout/footer.php'; ?>
<script src="<?php echo PROOT ?>/amc/js/update.js"></script> 