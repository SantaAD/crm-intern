<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';

require_once '../libs/database/database.php';
require_once './interfaces/AmcInterface.php';
require_once './Model/Amc.php';
require_once './Model/AmcProduct.php';
require_once '../customer/Customer.php';
require_once '../sales/interfaces/interface_sale.php';
require_once '../sales/classes/sale.php';
require_once '../items/classes/item.php';

$amc = new AMC();

if($_GET['view']){
    $id = $_GET['view'];
    //print_r($id);die;
    try{
        $amc = $amc->findById($id);
        //print_r($amc);die;
        $customer_details = $amc->getCustDetails();
    
        $sale_id = $amc->sale_id;
        //print_r($sale_id);die;
    
        if($sale_id == 'zero'){
            $amc_product = $amc->getAmcProdDetails(); 
      
        }else{
            $items = $amc->getItems($sale_id);
            //var_dump($items);
            //echo '<pre>';
            //print_r($items);  
        }
    }catch(Exception $e){
        echo 'Error Message: '.$e->getMessage();
    }
}

    //$amc = $amc->amcReportAll();

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                                <button onclick="history.go(-1)" class="btn btn-inverse btn-mat waves-effect waves-light ">Back</button>
                            <div class="card">
                                <div class="card-block">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>AMC Details</h4>
                                        </div>
                                        <div class="card-block">
                                            <form>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Amc ID</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $id?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Sale ID</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->sale_id?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Start Date</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->start_date?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">End Date</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->end_date?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Amount</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->amount?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Status</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->status?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>AMC Customer Details</h4>
                                        </div>
                                        <div class="card-block">
                                            <form>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Customer ID</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->customer_id?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Customer Name</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer_details->name?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Address</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $amc->getAddress()?>
                                                                </strong></label>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Email</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer_details->email?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label class="col-lg-4 col-form-label">Phone</label>
                                                        <label class="col-lg-6 col-form-label"><strong><?php echo $customer_details->phone?>
                                                                </strong></label>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>AMC Product Details</h4>
                                        </div>
                                        <div class="card-block">
                                            <div class="dt-responsive table-responsive table-sm">
                                                <table id="" class="table table-hover table-bordered nowrap">
                                                    <thead>
                                                    <?php if(isset($items)):?>
                                                        <tr>
                                                            <th>Sl. No.</th>
                                                            <th>Name</th>
                                                            <th>Price</th>
                                                            <th>Size</th>
                                                            <th>Model</th>
                                                            <th>Serial No</th>
                                                            <th>Barcode</th>
                                                            <th>GST</th>
                                                        </tr>
                                                    <?php elseif($amc_product):?>
                                                        <tr>
                                                            <th>Sl. No.</th>
                                                            <th>Name</th>
                                                            <th>Model</th>
                                                            <th>Serial No</th>
                                                            <th>Price</th>
                                                            <th>Purchase Date</th>
                                                            <th>Manufactured By</th>
                                                            <th>Warranty</th>
                                                            <th>Barcode</th>
                                                        </tr>
                                                    <?php endif;?>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(isset($items)):?>
                                                    <?php foreach($items as $item):?>
                                                    <?php foreach($item as $product):?>
                                                        <tr>
                                                            <td><?php echo $product->id?></td>
                                                            <td><?php echo $product->name?></td>
                                                            <td><?php echo $product->price?></td>
                                                            <td><?php echo $product->size?></td>
                                                            <td><?php echo $product->model?></td>
                                                            <td><?php echo $product->serial_no?></td>
                                                            <td><?php echo $product->barcode?></td>
                                                            <td><?php echo $product->gst?></td>
                                                    <?php endforeach;?>
                                                    <?php endforeach;?>
                                                    <?php elseif($amc_product):?>
                                                    <?php foreach($amc_product as $product):?>
                                                    
                                                        <tr>
                                                            <td><?php echo $product->id?></td>
                                                            <td><?php echo $product->name?></td>
                                                            <td><?php echo $product->model?></td>
                                                            <td><?php echo $product->serial?></td>
                                                            <td><?php echo $product->price?></td>
                                                            <td><?php echo $product->purchase_date?></td>
                                                            <td><?php echo $product->manufactured_by?></td>
                                                            <td><?php echo $product->warranty?></td>
                                                            <td><?php echo $product->barcode?></td>
                                                    <?php endforeach;?>
                                                    <?php endif;?>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
    include '../layout/footer.php';
?>