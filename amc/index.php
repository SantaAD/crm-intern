<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
include '../layout/header.php';
include '../layout/sidebar.php';

require_once '../libs/database/database.php';
require_once './interfaces/AmcInterface.php';
require_once './Model/Amc.php';
require_once '../customer/Customer.php';

$amc = new AMC();


if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    try{
        $amc = $amc->findById($id);

        $delete_res = $amc->delete();
        echo '<script>alert("AMC deleted successfully); window.location = "./index.php"</script>';
    }catch(Exception $e){
        echo '<script>alert("AMC failed to delete); window.location = "./index.php"</script>';
    }
}

if (isset($_POST['submit'])) {
    $name = $_POST['select'];
    $search_value = $_POST['search'];
    $from = $_POST['from'];
    $to = $_POST['to'];

    try{
        if ($name == 'name') {
            $amci = $amc->getAmcByName($search_value);
            //$amcinfo;
            //echo "<pre>";
            //print_r($amcinfo);die;
        } elseif ($name == 'phone') {
            $amci = $amc->getAmcByph($search_value);
        } elseif ($name == 'address') {
            $amci = $amc->getAmcByAdd($search_value);
        } elseif ($name == 'product') {
            $amci = $amc->getAmc($search_value);
            //echo "<pre>";
            //print_r($amci);die;
        } elseif ($from && $to) {
            $amcinfo = $amc->amcReport($from, $to);
            //echo "<pre>";
            //print_r($amcinfo);die;
            //print_r($amcinfo);die;
        }
    }catch(Exception $e){
        echo 'Error Message: '.$e->getMessage();
    }
} else {
    try{
        $amcinfo = $amc->amcReportAll();
    }catch(Exception $e){
        echo 'Error Message: '.$e->getMessage();
    }
    //echo "<pre>";
    //print_r($amcinfo);die;
}

//$amcinfo = $amc->amcReportAll();


//$amcs = $amc->findById($amcinfo->id);
?>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="row">
                                        <div class="col col-lg-3">
                                              <a href="./amcnew.php"><button class="btn btn-mat btn-inverse">Add
                                                        AMC</button></a>
                                        </div>
                                        <div class="col col-lg-9">
                                               <h3 style="float:right">AMC Details</h3>
                                        </div>
                                    </div>
                            <div class="page-header-title">
                                <div class="d-inline">
                                   

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="./js/amc.js">

                </script>


                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Filter options</h5>
                                    <form action="" method="POST">

                                        <div class="form-group row">
                                            <div class="col col-sm-12 col-md-2">
                                                <select name="select" id="select" class="form-control sm">
                                                    <option value="">Select Option</option>
                                                    <option value="name">Name</option>
                                                    <option value="phone">Phone</option>
                                                    <option value="product">Product</option>
                                                    <option value="address">Address</option>
                                                </select>
                                            </div>

                                            <div class="col col-sm-12 col-md-4">
                                                <input type="text" class="form-control sm" placeholder="Search" name="search">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <input type="text" class="form-control sm" placeholder="From" name="from">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <input type="text" class="form-control sm" placeholder="To" name="to">
                                            </div>

                                            <div class="col col-sm-12 col-md-2">
                                                <button name="submit" class="btn btn-inverse btn-sm btn-mat" style="font-size:18px;"><i class="feather icon-filter"></i></button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive table-sm">

                                        <table id="simpletable" class="table table-bordered nowrap table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Customer</th>
                                                    <th>Amount</th>
                                                    <th>Start date</th>
                                                    <th>End date</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($amci)) : ?>
                                                <?php foreach ($amci as $amc) : ?>
                                                <?php foreach ($amc as $amcdetail) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $amcdetail->id ?>
                                                    </td>
                                                    <td>
                                                    <a class="text-info" href="<?php echo PROOT?>/customer/profile.php?profileview=<?php echo $amcdetail->customer_id?>">
                                                            <?php echo $amcdetail->findCustName() ?></a></td>
                                                    <td>
                                                        <?php echo $amcdetail->amount ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $amcdetail->start_date ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $amcdetail->end_date ?>
                                                    </td>
                                                    <?php  ?>
                                                    <?php  ?>
                                                    <?php
                                                    if ($amcdetail->status == 'Active') {
                                                        $status = "Active";
                                                        $text = "success";
                                                    } elseif ($amcdetail->status == 'Terminated') {
                                                        $status = "Terminated";
                                                        $text = "danger";
                                                    } elseif ($amcdetail->status == 'Expired') {
                                                        $status = "Expired";
                                                        $text = "warning";
                                                    }
                                                    ?>
                                                    <td><span class="label label-<?php echo $text ?>"><?php echo $status ?></span></td>
                                                    <td>
                                                        <div class="dropdown-inverse dropdown open">
                                                            <button class="btn btn-mat btn-inverse dropdown-toggle waves-effect waves-light " type="button" id="dropdown-7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdown-7" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                                <a class="dropdown-item waves-light waves-effect" href="./amcview.php?view=<?php echo $amcdetail->id ?>"><i class="text-success feather icon-eye"></i>View</a>
                                                                <a class="dropdown-item waves-light waves-effect" onclick="return confirm('You sure want to delete?')" href="<?php echo $_SERVER['PHP_SELF'] ?>??del=<?php echo $amcdetail->id; ?> "><i class="text-danger feather icon-trash-2"></i>Delete</a>
                                                                <a class="dropdown-item waves-light waves-effect" href="./amcupdate.php?update=<?php echo $amcdetail->id ?>"><i class="text-success feather icon-edit "></i>Update</a>
                                                            </div>
                                                        </div>
                                    </div>
                                    </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endforeach; ?>
                                    <?php elseif (isset($amcinfo)) : ?>
                                    <?php foreach ($amcinfo as $amcdetail) : ?>
                                    <tr>
                                        <td>
                                            <?php echo $amcdetail->id ?>
                                        </td>
                                        <td><a class="text-info" href="<?php echo PROOT?>/customer/profile.php?profileview=<?php echo $amcdetail->customer_id?>">
                                                <?php echo $amcdetail->findCustName() ?></a></td>
                                        <td>
                                            <?php echo $amcdetail->amount ?>
                                        </td>
                                        <td>
                                            <?php echo $amcdetail->start_date ?>
                                        </td>
                                        <td>
                                            <?php echo $amcdetail->end_date ?>
                                        </td>
                                        <?php  ?>
                                        <?php  ?>
                                        <?php
                                        if ($amcdetail->status == 'Active') {
                                            $status = "Active";
                                            $text = "success";
                                        } elseif ($amcdetail->status == 'Terminated') {
                                            $status = "Terminated";
                                            $text = "danger";
                                        } elseif ($amcdetail->status == 'Expired') {
                                            $status = "Expired";
                                            $text = "warning";
                                        }
                                        ?>
                                        <td><span class="label label-<?php echo $text ?>"><?php echo $status ?></span></td>
                                        <td>
                                            <div class="dropdown-inverse dropdown open">
                                                <button class="btn btn-mat btn-inverse dropdown-toggle waves-effect waves-light " type="button" id="dropdown-7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action</button>
                                                <div class="dropdown-menu" aria-labelledby="dropdown-7" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                                    <a class="dropdown-item waves-light waves-effect" href="./amcview.php?view=<?php echo $amcdetail->id ?>"><i class="text-success feather icon-eye"></i>View</a>
                                                    <a class="dropdown-item waves-light waves-effect" onclick="return confirm('You sure want to delete?')" href="<?php echo $_SERVER['PHP_SELF'] ?>?delete=<?php echo $amcdetail->id ?>"><i class="text-danger feather icon-trash-2"></i>Delete</a>
                                                    <a class="dropdown-item waves-light waves-effect" href="./amcupdate.php?update=<?php echo $amcdetail->id ?>"><i class="text-success feather icon-edit "></i>Update</a>
                                                </div>
                                            </div>
                                </div>
                                </td>
                                </tr>

                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>



<?php
include '../layout/footer.php';
?>

<script>

</script> 
