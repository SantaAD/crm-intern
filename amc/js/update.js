const amc_update = document.getElementById('update');
amc_update.addEventListener('submit', function (e) {
    e.preventDefault();
    let id = document.getElementById('id');
    let start_date = document.getElementById('start_date');
    let end_date = document.getElementById('end_date');
    let amount = document.getElementById('amount');
    let msg = document.getElementById('msg');
    let submit = document.getElementById('submit');

    var params = new URLSearchParams(); //
    params.append('id', id.value);
    params.append('start_date', start_date.value);
    params.append('end_date', end_date.value);
    params.append('amount', amount.value);

    axios.post(config.host + '/amc/amcs/update.php', params)
        .then(function (response) {

            if (response.data.status == 'success') {
                submit.innerHTML = 'Submit';
               alert("AMC details updated successfully");
            } else {
                submit.innerHTML = 'Submit';
                alert("Failed updated AMC details");
            }

        })
        .catch(function (error) {
            submit.innerHTML = 'submit';
            console.log(error);
        });
})