<?php

ini_set('display_errors', 1);
  
if(!isset($_REQUEST['start_date']) || empty($_REQUEST['start_date'])) {
    $array = array(
        "msg" => "Enter Date..",
        "status" => "Failed!"
    );
    echo json_encode($array);
    return;
}

if(!isset($_REQUEST['end_date']) || empty($_REQUEST['end_date'])) {
    $array = array(
        "msg" => "Enter Date..",
        "status" => "Failed!"
    );
    echo json_encode($array);
    return;
}

if(!isset($_REQUEST['amount']) || empty($_REQUEST['amount'])) {
    $array = array(
        "msg" => "Enter the Amount..",
        "status" => "Failed!"
    );
    echo json_encode($array);
    return;
}

require_once '../../libs/database/database.php';
require_once '../interfaces/AmcInterface.php';
require_once '../Model/Amc.php';

$amc = new AMC($_REQUEST);
$amc->update($_REQUEST);
echo json_encode(['status'=> 'success', 'msg' => 'Amc Updated']);

?> 