<?php
/**
 * Interface AMC 
 * Mofiqul Islam, mofi0islam@gmail.com 
 * Created at : 24-02-2019 8:56PM
 */
interface interfaceAMC
{

    /**
     * Saves a AMC to the database
     * @return ${AMC:AMC}
     */
    public function save();

    /**
     * Updateds a exiting AMC
     * @param ${data:array}
     */
    public function update($data);

    /**
     * Delete a AMC
     * @param ${id:integer}
     * @return ${AMC:AMC}
     */
    public function delete();

    /**
     * Find AMC with key value pair
     * @param ${id:integer}
     */
    public  function findByID($id);

}