<?php 
use PHPUnit\Framework\MockObject\Stub\Exception;

require_once('../../libs/database/database.php');
require_once('../../sales/interfaces/interface_sale.php');
require_once('../../sales/classes/sale.php');


$sale = new Sale();
var_dump($sale->get());
die;

$sale_data = [
    'customer_id' => '100000'
];

$sale = new Sale($sale_data);

try{
    $sale->save();
    echo "Passed: Sale save".PHP_EOL;
} catch(Exception $e){
    echo $e->getMessage();
    echo "Failed: Sale save".PHP_EOL;
}

$s = $sale->findByID(100000);

try{
    $s->update(['customer_id' => '100001', 'field' => 'value']);
    echo "Passed: Sale update".PHP_EOL;
} catch(Exception $e){
    echo $e->getMessage();
    echo "Failed: Sale update";
}

if($sale->find(['customer_id' => '100001'])){
    echo "Passed: Sale find".PHP_EOL;
} else{
    echo "Failed: Sale find".PHP_EOL;
}






