<?php

require_once('../../libs/database/database.php');
require_once('../../sales/interfaces/AmcInterface.php');
require_once('../../sales/Model/Amc.php');

$amc = new AMC([
    'start_date' => '01-01-2018',
    'end_date' => '01-01-2019',
    'amount' => '',
    'customer_id' => '10000',
    'sale_id' => '1'
]);

try{
    $amc->save();
} catch(Exception $e){
    echo $e->getMessage();
}