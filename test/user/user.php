<?php
require_once('../../auth/User.php');

$u = [
    'username' => 'harry',
    'password' => 'password',
    'role' => 'admin',
    'name' => 'Harry',
    'mobile' => '9876543210',
    'email' => 'example@email.com',
    'designation' => 'Woner'
];

$user = new User($u);
print_r($user->save());