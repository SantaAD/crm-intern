-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 06, 2019 at 12:39 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(150) NOT NULL DEFAULT 'Guwahati',
  `pin` varchar(15) NOT NULL DEFAULT '781021',
  `customer_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `address`, `city`, `pin`, `customer_id`) VALUES
(109, 'address', 'Guwahati', '781021', '100000'),
(110, 'address', 'Guwahati', '781021', '100001'),
(111, 'address', 'Guwahati', '781021', '100002'),
(112, 'address', 'Guwahati', '781021', '100003');

-- --------------------------------------------------------

--
-- Table structure for table `amc`
--

CREATE TABLE `amc` (
  `id` varchar(15) NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'Active',
  `sale_id` varchar(15) NOT NULL,
  `customer_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amc`
--

INSERT INTO `amc` (`id`, `start_date`, `end_date`, `amount`, `status`, `sale_id`, `customer_id`) VALUES
('100007', '10-10-2019', '10-10-2022', '6666', 'Active', 'zero', '100000'),
('100008', '12-12-2019', '12-12-2020', '77888', 'Active', '100015', '100000');

-- --------------------------------------------------------

--
-- Table structure for table `amc_product`
--

CREATE TABLE `amc_product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `price` varchar(15) NOT NULL,
  `quantity` int(50) NOT NULL,
  `purchase_date` varchar(50) NOT NULL,
  `warranty` varchar(50) NOT NULL,
  `manufactured_by` varchar(50) NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `amc_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amc_product`
--

INSERT INTO `amc_product` (`id`, `name`, `model`, `serial`, `price`, `quantity`, `purchase_date`, `warranty`, `manufactured_by`, `barcode`, `amc_id`) VALUES
(1, 'dgheuy', 'uyuyu', 'ytutyuytuyt', '$567567', 7, '10-10-2019', '5', 'hgjhgj', '65746567657', '100007'),
(2, 'dgheuy', 'uyuyu', 'ytutyuytuyt', '$567567', 7, '10-10-2019', '5', 'hgjhgj', '65746567657', '100007'),
(3, 'dgheuy', 'uyuyu', 'ytutyuytuyt', '$567567', 7, '10-10-2019', '5', 'hgjhgj', '65746567657', '100007');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` varchar(15) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gstin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `email`, `name`, `phone`, `gstin`) VALUES
('100000', 'mofi0islam@gmail.com', 'Mofiqul Islam', '8486547139', '8978675645'),
('100001', 'mofi0islam@gmail.com', 'Mofiqul Islam', '8486547139', '8978675645'),
('100002', 'mofi0islam@gmail.com', 'Kangkan Rajkhowa', '8486547139', '8978675645'),
('100003', 'mofi0islam@gmail.com', 'Kangkan Rajkhowa', '8486547139', '8978675645');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `price` varchar(15) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `serial_no` varchar(50) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `gst` varchar(10) NOT NULL DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `size`, `model`, `serial_no`, `barcode`, `gst`) VALUES
(1, 'item1', '1000', '1 KG', '', '', '1234567890', '18'),
(2, 'item2', '1020', '4 Packet', '', '', '1234567891', '18'),
(3, 'item3', '1220', '100Mg', '', '', '1234567892', '18'),
(4, 'item4', '1320', '1 \r\n  Bottle', '', '', '1234567893', '18'),
(5, 'item5', '1350', '1 Box', '', '', '1234567894', '18');

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `id` varchar(15) NOT NULL,
  `sub_total` varchar(15) NOT NULL,
  `gst_total` varchar(15) NOT NULL,
  `customer_id` varchar(15) NOT NULL,
  `total` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`id`, `sub_total`, `gst_total`, `customer_id`, `total`, `date`) VALUES
('100015', '3,240.00', '41,639.84', '100001', '44,879.84', '2019-03-03 13:16:38'),
('100021', '4,560.00', '246,945.68', '100001', '251,505.68', '2019-03-03 13:45:46'),
('100022', '4,560.00', '246,945.68', '100001', '251,505.68', '2019-03-03 13:45:46'),
('100023', '4,560.00', '246,945.68', '100001', '251,505.68', '2019-03-03 15:39:54'),
('100026', '4,560.00', '62,200.16', '100001', '66,760.16', '2019-03-05 07:21:07'),
('100027', '2,670.00', '42,065.82', '100003', '44,735.82', '2019-03-05 07:21:23'),
('100028', '2,670.00', '42,065.82', '100003', '44,735.82', '2019-03-05 07:21:23'),
('100029', '4,910.00', '71,905.66', '100001', '76,815.66', '2019-03-05 07:21:43'),
('100030', '1,000.00', '180.00', '100001', '1,180.00', '2019-03-05 13:02:42'),
('100031', '2,240.00', '403.20', '100001', '2,643.20', '2019-03-05 13:04:03'),
('100032', '5,000.00', '900.00', '100001', '5,900.00', '2019-03-05 13:04:24'),
('100034', '20,030.00', '3,605.40', '100001', '23,635.40', '2019-03-05 13:06:09'),
('100035', '2,020.00', '363.60', '100001', '2,383.60', '2019-03-05 13:13:31'),
('100036', '2,240.00', '403.20', '100001', '2,643.20', '2019-03-05 13:13:42'),
('100037', '2,020.00', '363.60', '100001', '2,383.60', '2019-03-05 13:13:56'),
('100038', '2,670.00', '480.60', '100001', '3,150.60', '2019-03-05 13:14:06'),
('100039', '2,240.00', '403.20', '100001', '2,643.20', '2019-03-05 13:14:25'),
('100040', '4,910.00', '883.80', '100001', '5,793.80', '2019-03-05 13:14:34');

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `id` int(11) NOT NULL,
  `item_id` varchar(15) DEFAULT NULL,
  `quantity` varchar(10) NOT NULL,
  `sale_id` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`id`, `item_id`, `quantity`, `sale_id`) VALUES
(11, '1', '1', '100015'),
(12, '2', '1', '100015'),
(13, '3', '1', '100015'),
(14, '1', '1', '100016'),
(15, '2', '1', '100016'),
(16, '1', '4', '100017'),
(17, '2', '3', '100017'),
(18, '3', '1', '100017'),
(19, '1', '4', '100018'),
(20, '2', '3', '100018'),
(21, '3', '1', '100018'),
(22, '2', '1', '100019'),
(23, '3', '1', '100019'),
(24, '4', '1', '100019'),
(25, '1', '1', '100020'),
(26, '2', '1', '100020'),
(27, '3', '1', '100020'),
(28, '1', '1', '100021'),
(29, '1', '1', '100022'),
(30, '2', '6', '100021'),
(31, '2', '6', '100022'),
(32, '3', '1', '100021'),
(33, '3', '1', '100022'),
(34, '4', '7', '100021'),
(35, '4', '7', '100022'),
(36, '1', '1', '100023'),
(37, '2', '6', '100023'),
(38, '3', '1', '100023'),
(39, '4', '7', '100023'),
(40, '1', '1', '100024'),
(41, '1', '1', '100025'),
(42, '1', '1', '100026'),
(43, '2', '1', '100025'),
(44, '2', '1', '100026'),
(45, '3', '1', '100025'),
(46, '3', '1', '100026'),
(47, '4', '1', '100025'),
(48, '4', '1', '100026'),
(49, '5', '1', '100027'),
(50, '5', '1', '100028'),
(51, '4', '1', '100027'),
(52, '4', '1', '100028'),
(53, '2', '1', '100030'),
(54, '2', '1', '100029'),
(55, '3', '1', '100030'),
(56, '3', '1', '100029'),
(57, '4', '1', '100030'),
(58, '4', '1', '100029'),
(59, '5', '1', '100030'),
(60, '5', '1', '100029'),
(61, '1', '1', '100030'),
(62, '2', '1', '100031'),
(63, '3', '1', '100031'),
(64, '1', '5', '100032'),
(65, '5', '5', '100033'),
(66, '3', '4', '100033'),
(67, '2', '2', '100033'),
(68, '5', '3', '100034'),
(69, '3', '9', '100034'),
(70, '1', '5', '100034'),
(71, '1', '1', '100035'),
(72, '2', '1', '100035'),
(73, '2', '1', '100036'),
(74, '3', '1', '100036'),
(75, '1', '1', '100037'),
(76, '2', '1', '100037'),
(77, '5', '1', '100038'),
(78, '4', '1', '100038'),
(79, '2', '1', '100039'),
(80, '3', '1', '100039'),
(81, '2', '1', '100040'),
(82, '3', '1', '100040'),
(83, '4', '1', '100040'),
(84, '5', '1', '100040'),
(85, '1', '1', '100041'),
(86, '3', '1', '100041'),
(87, '4', '1', '100041'),
(88, '5', '1', '100041'),
(89, '1', '1', '100042'),
(90, '2', '1', '100042'),
(91, '3', '1', '100042'),
(92, '4', '1', '100042'),
(93, '1', '1', '100043'),
(94, '4', '1', '100043'),
(95, '3', '1', '100043'),
(96, '2', '1', '100043');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(150) NOT NULL,
  `role` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `role`, `email`, `mobile`, `designation`, `created_at`) VALUES
(5, 'harry', '$2y$10$OOWSljvCLSKgL5Fh/Z8mtO/PDXXrf6pXK0leo/Q0GSKHnYjaOR5Kq', 'Harry', 'admin', 'example@email.com', '9876543210', 'Woner', '2019-02-23 10:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `warranty_period`
--

CREATE TABLE `warranty_period` (
  `id` int(11) NOT NULL,
  `period` varchar(20) NOT NULL,
  `display` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warranty_period`
--

INSERT INTO `warranty_period` (`id`, `period`, `display`) VALUES
(1, '3', '3 Months'),
(2, '6', '6 Months'),
(3, '12', '12 Months'),
(4, '18', '18 Months'),
(5, '24', '24 Months');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amc`
--
ALTER TABLE `amc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amc_product`
--
ALTER TABLE `amc_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warranty_period`
--
ALTER TABLE `warranty_period`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `amc_product`
--
ALTER TABLE `amc_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `warranty_period`
--
ALTER TABLE `warranty_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
